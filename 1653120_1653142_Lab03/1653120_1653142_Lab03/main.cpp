#include "Lab03.h"

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		cout << "Tham so khong hop le\n";
		return -1;
	}
	string imgName;
	Mat img, image;
	img = imread(argv[1],IMREAD_UNCHANGED);
	if (!img.data)
	{
		cout << "Khong the mo anh" << std::endl;
		return -1;
	}
	if (img.type() != CV_8UC1 && img.type() != CV_8UC3)
	{
		cout << "Hinh anh dau vao khong phai loai rgb hay grayscale\n";
		return -1;
	}
	if (strcmp(argv[2], "1") == 0) //robert
	{
		imgName = "Robert Filter";
		robertFilter(img, image);
	}

	if (strcmp(argv[2], "2") == 0) //sobel
	{
		imgName = "Sobel Filter";
		sobelFilter(img, image);
	}

	if (strcmp(argv[2], "3") == 0) //Laplacian
	{
		imgName = "Laplacian Filter";
		laplacianFilter(img, image);
	}

	if (strcmp(argv[2], "4") == 0) //unsharp
	{
		imgName = "Unsharp masking filter";
		int k = atoi(argv[3]);
		unsharpMaskingProc(img, image, k);
	}
	
	namedWindow("Source", WINDOW_AUTOSIZE);
	imshow("Source", img);
	namedWindow(imgName, WINDOW_AUTOSIZE);
	imshow(imgName, image);
	waitKey(0);
	return 0;
}