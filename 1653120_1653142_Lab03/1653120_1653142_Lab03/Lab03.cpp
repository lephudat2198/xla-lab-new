﻿#include "Lab03.h"

constexpr auto nMask = 3;

//padding ảnh theo  
//w: chiều dài
//h: chiều cao
//Option: 
//1: replating
//0: zero
void convertImagePadding(Mat sourceImage, Mat & destinationImage, int option, int w, int h)
{
	//Chiều dài ảnh sẽ được cộng 2 lần h
	//Chiều rộng ảnh sẽ được cộng 2 lần w
	int newRows, newCols;
	newRows = sourceImage.rows + h * 2;
	newCols = sourceImage.cols + w * 2;
	Mat tmp(newRows, newCols, sourceImage.type(), Scalar(0));
	if (option == 0)
	{
		if (sourceImage.channels() == 1)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(sourceImage.at<uchar>(i - h, j - w));
				}
			}
		}
		if (sourceImage.channels() == 3)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = saturate_cast<uchar>(
							sourceImage.at<Vec3b>(i - h, j - w)[k]);
					}
				}
			}
		}
		destinationImage = tmp;
		return;
	}
	if (option == 1)
	{
		if (sourceImage.channels() == 1)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(sourceImage.at<uchar>(i - h, j - w));
				}
			}
			//Xử lí viền dưới
			for (int i = newRows - h; i < newRows; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						sourceImage.at<uchar>(sourceImage.rows - 1, c));
					c++;
				}
			}
			//Xử lí viền trên
			for (int i = 0; i < h; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						sourceImage.at<uchar>(0, c));
					c++;
				}
			}
			//Xử lí viền trái
			for (int i = 0; i < newRows; i++)
			{
				for (int j = 0; j < w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						tmp.at<uchar>(i, w));
				}
			}
			//xử lí viền bên phải
			for (int i = 0; i < newRows; i++)
			{
				for (int j = newCols - w; j < newCols; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						tmp.at<uchar>(i, newCols - w - 1));
				}
			}
		}
		if (sourceImage.channels() == 3)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = saturate_cast<uchar>(
							sourceImage.at<Vec3b>(i - h, j - w)[k]);
					}
				}
			}

			//Xử lí viền dưới
			for (int i = newRows - h; i < newRows; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] =
							sourceImage.at<Vec3b>(sourceImage.rows - 1, c)[k];
					}
					c++;
				}
			}
			//Xử lí viền trên
			for (int i = 0; i < h; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = (
							sourceImage.at < Vec3b>(0, c)[k]);
					}
					c++;
				}
			}
			//Xử lí viền trái
			for (int i = 0; i < newRows; i++)
			{
				for (int j = 0; j < w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = tmp.at<Vec3b>(i, w)[k];
					}
				}
			}
			//xử lí viền bên phải
			for (int i = 0; i < newRows; i++)
			{
				for (int j = newCols - w; j < newCols; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = (
							tmp.at<Vec3b>(i, newCols - w - 1)[k]);
					}
				}
			}
		}
		destinationImage = tmp;
		return;
	}
}

//Drop ảnh vừa size của ảnh gốc
void dropImage(Mat sourceImage, Mat & destinationImage, int w, int h)
{
	//Xóa các cạnh thêm với 2 lần w, 2 lần h
	Mat tmp(sourceImage.rows - h * 2, sourceImage.cols - 2 * w, sourceImage.type());

	//Gán lại giá trị
	if (sourceImage.channels() == 1)
	{
		for (int i = 0; i < tmp.rows; i++)
		{
			for (int j = 0; j < tmp.cols; j++)
			{
				tmp.at<uchar>(i, j) =
					saturate_cast<uchar>(sourceImage.at<uchar>(i + h, j + w));
			}
		}
	}
	if (sourceImage.channels() == 3)
	{
		int valueI, valueJ;
		for (int i = 0; i < tmp.rows; i++)
		{
			for (int j = 0; j < tmp.cols; j++)
			{
				valueI = i + h, valueJ = j + w;
				tmp.at<Vec3b>(i, j)[0] = saturate_cast<uchar>(
					sourceImage.at<Vec3b>(valueI, valueJ)[0]);

				tmp.at<Vec3b>(i, j)[1] = saturate_cast<uchar>(
					sourceImage.at<Vec3b>(valueI, valueJ)[1]);

				tmp.at<Vec3b>(i, j)[2] = saturate_cast<uchar>(
					sourceImage.at<Vec3b>(valueI, valueJ)[2]);
			}
		}
		destinationImage = tmp;
	}
	destinationImage = tmp;
}

//Lấy các giá trị láng riềng của điểm center
//Trả về mảng 2 chiều các giá trị theo channels 0|1234567....
//w: chiều dài
//h: chiều cao
//option 1 - ascend array
//option 2 - normal array
vector<vector<int>> getNeighbors(Mat sourceImg, int x, int y, int w, int h, int option)
{
	////Concept:
	//	Đầu vào là 1 pixel trên ảnh
	//	Đầu ra sẽ trả về mảng các giá trị xung quanh của 1 pixel truy vấn,
	//		tùy theo loại ảnh xám sẽ có 1 mảng các giá trị
	//		màu RGB sẽ có 3 mảng các giá trị
	//	Những pixel không thể áp filter w x h lên được sẽ trả về rỗng.

	//mảng các giá trị xung quanh của 1 pixel truy vấn
	vector<vector<int>> re;

	//Trả về rỗng với những điểm không áp filter được
	if (x < h / 2 - 0		//những điểm rìa dọc trái ( 0,1; 1,1; 2,1 .... (h/2 - 1),1)
		|| y < w / 2 - 0			//những điểm rìa trên ( 0,1; 0,2; 0,3 .... 0,(w/2-1))
		|| x > sourceImg.rows - 1 - h / 2			//những điểm rìa rìa dưới
		|| y > sourceImg.cols - 1 - w / 2)			////những điểm dọc phải
		return vector<vector<int>>();

	//Xử lí theo loại ảnh
	switch (sourceImg.channels())
	{
	case 1://Grayscale
	{
		vector<int> gArr;
		int i, k, j, ni, nj, value;
		bool isChanged = false;

		i = (x - h / 2);
		ni = (x + h / 2);
		nj = (y + w / 2);
		k = (y - w / 2);

		for (i; i <= ni; i++)
		{
			j = k;
			for (j; j <= nj; j++)
			{
				switch (option)
				{
				case 1://ascend
				{

					value = (int)sourceImg.at<uchar>(i, j);
					if (gArr.empty())
					{
						gArr.push_back(value);
					}
					else
					{
						vector<int>::iterator c;
						for (c = gArr.begin(); c < gArr.end(); c++)
						{
							if (value < *c)
							{
								gArr.insert(c, value);
								break;
							}

						}

						if (c == gArr.end())
							gArr.insert(gArr.end(), value);
					}
				}
				break;
				case 2://normal
				{
					gArr.push_back((int)sourceImg.at<uchar>(i, j));
				}
				break;
				default:
					break;
				}
			}
		}

		re.push_back(gArr);
		return re;
	}
	break;
	case 3://RGB
	{
		vector<int> gArr1, gArr2, gArr3;
		int i, k, j, ni, nj;
		int value1, value2, value3;

		i = (x - h / 2);
		ni = (x + h / 2);
		nj = (y + w / 2);
		k = (y - w / 2);

		for (i; i <= ni; i++)
		{
			j = k;
			for (j; j <= nj; j++)
			{
				switch (option)
				{
				case 1://ascend
				{
					value1 = sourceImg.at<Vec3b>(i, j)[0];
					value2 = sourceImg.at<Vec3b>(i, j)[1];
					value3 = sourceImg.at<Vec3b>(i, j)[2];

					if (gArr1.empty())
					{
						gArr1.push_back(value1);
					}
					else
					{
						vector<int>::iterator c1;
						for (c1 = gArr1.begin(); c1 < gArr1.end(); c1++)
						{
							if (value1 < *c1)
							{
								gArr1.insert(c1, value1);
								break;
							}
						}
						if (c1 == gArr1.end())
							gArr1.insert(gArr1.end(), value1);
					}

					if (gArr2.empty())
					{
						gArr2.push_back(value2);
					}
					else
					{
						vector<int>::iterator c2;
						for (c2 = gArr2.begin(); c2 < gArr2.end(); c2++)
						{
							if (value2 < *c2)
							{
								gArr2.insert(c2, value2);
								break;
							}
						}
						if (c2 == gArr2.end())
							gArr2.insert(gArr2.end(), value2);
					}

					if (gArr3.empty())
					{
						gArr3.push_back(value3);
					}
					else
					{
						vector<int>::iterator c3;
						for (c3 = gArr3.begin(); c3 < gArr3.end(); c3++)
						{
							if (value3 < *c3)
							{
								gArr3.insert(c3, value3);
								break;
							}
						}
						if (c3 == gArr3.end())
							gArr3.insert(gArr3.end(), value3);
					}

				}
				break;
				case 2://normal
				{
					gArr1.push_back((int)sourceImg.at<Vec3b>(i, j)[0]);
					gArr2.push_back((int)sourceImg.at<Vec3b>(i, j)[1]);
					gArr3.push_back((int)sourceImg.at<Vec3b>(i, j)[2]);
				}
				break;
				default:
					break;
				}

			}
		}
		//vec(gArr1);
		/*vec(gArr2);
		vec(gArr3);*/
		re.push_back(gArr1);
		re.push_back(gArr2);
		re.push_back(gArr3);
		return re;
	}
	break;
	default:
		break;
	}
	return re;
}

//Bước tiền xử lí cho bộ lọc Robert
vector <vector<int>> CreateRobertMask()
{
	vector<int> Gx;
	vector<int> Gy;
	vector<vector<int>> Mask; //Tạo 1 vector 2-dimensions lưu trữ 2 bộ lọc Gx và Gy.
	//Tạo bộ lọc Gx theo thuật toán Robert
	// 0 0 0
	// 0 -1 0
	// 0 0 1
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(-1);
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(1);
	Mask.push_back(Gx);
	//Tạo bộ lọc Gy theo thuật toán Robert
	// 0 0 0
	// 0 0 -1
	// 0 1 0
	Gy.push_back(0);
	Gy.push_back(0);
	Gy.push_back(0);
	Gy.push_back(0);
	Gy.push_back(0);
	Gy.push_back(-1);
	Gy.push_back(0);
	Gy.push_back(1);
	Gy.push_back(0);
	Mask.push_back(Gy);
	return Mask;
}

//Bộ lọc Robert
int robertFilter(Mat sourceImage, Mat &destinationImage)
{
	Mat imgPadding;

	int gx, gy, gx2, gy2, gx3, gy3;
	convertImagePadding(sourceImage, imgPadding, 1, 3, 3);
	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());
	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < imgPadding.rows; i++)	
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, 3, 3, 2);
				vector<vector<int>> RobertMask = CreateRobertMask();
				if (!neighbor.empty())
				{
					gx = 0, gy = 0;
					for (int k = 0; k < neighbor[0].size(); k++)
					{
						gx += neighbor[0][k] * RobertMask[0][k];
						gy += neighbor[0][k] * RobertMask[1][k];
					}
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(sqrt(gx*gx + gy*gy)/2);
				}
			}
		}
	}
	break;
	case 3://ảnh màu
	{
		vector<vector<int>> RobertMask = CreateRobertMask();
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, 3, 3, 2);
				if (!neighbor.empty())
				{
					gx = gx2 = gx3 = 0;
					gy = gy2 = gy3 = 0;

					for (int k = 0; k < neighbor[0].size(); k++)
					{
						gx += neighbor[0][k] * RobertMask[0][k];
						gx2 += neighbor[1][k] * RobertMask[0][k];
						gx3 += neighbor[2][k] * RobertMask[0][k];

						gy += neighbor[0][k] * RobertMask[1][k];
						gy2 += neighbor[1][k] * RobertMask[1][k];
						gy3 += neighbor[2][k] * RobertMask[1][k];
					}

					tmp.at<Vec3b>(i, j)[0] = saturate_cast<uchar>(sqrt(gx*gx + gy * gy) / 2);
					tmp.at<Vec3b>(i, j)[1] = saturate_cast<uchar>(sqrt(gx2*gx2 + gy2 * gy2) / 2);
					tmp.at<Vec3b>(i, j)[2] = saturate_cast<uchar>(sqrt(gx3*gx3 + gy3 * gy3) / 2);
					
				}
			}
		}
	}
	break;
	default:
		return -1;
		break;
	}
	dropImage(tmp, destinationImage, 3, 3);
	return 0;
}

//Bước tiền xử lí cho bộ lọc Sobel
vector <vector<int>> CreateSobelMask()
{
	vector<int> Gx;
	vector<int> Gy;
	vector<vector<int>> Mask; //Tạo 1 vector 2-dimensions lưu trữ 2 bộ lọc Gx và Gy.
	//Tạo bộ lọc Gx theo thuật toán Sobel
	// -1 -2 -1
	// 0 0 0
	// 1 2 1
	Gx.push_back(-1);
	Gx.push_back(-2);
	Gx.push_back(-1);
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(0);
	Gx.push_back(1);
	Gx.push_back(2);
	Gx.push_back(1);
	Mask.push_back(Gx);
	//Tạo bộ lọc Gy theo thuật toán Sobel
	// -1 0 1
	// -2 0 2
	// -1 0 1
	Gy.push_back(-1);
	Gy.push_back(0);
	Gy.push_back(1);
	Gy.push_back(-2);
	Gy.push_back(0);
	Gy.push_back(2);
	Gy.push_back(-1);
	Gy.push_back(0);
	Gy.push_back(1);
	Mask.push_back(Gy);
	return Mask;
}

//Bộ lọc Sobel
int sobelFilter(Mat sourceImage, Mat &destinationImage)
{
	//Tạo mask	3x3 cho laplacian filter
	int **maskx, **masky;
	maskx = new int*[nMask];
	masky = new int*[nMask];
	for (int i = 0; i < nMask; i++)
	{
		maskx[i] = new int[nMask];
		masky[i] = new int[nMask];
	}

	//Gán tĩnh giá trị cho mask
	maskx[0][0] = -1;
	maskx[0][1] = -2;
	maskx[0][2] = -1;
	maskx[1][0] = 0;
	maskx[1][1] = 0;
	maskx[1][2] = 0;
	maskx[2][0] = 1;
	maskx[2][1] = 2;
	maskx[2][2] = 1;

	masky[0][0] = -1;
	masky[0][1] = 0;
	masky[0][2] = 1;
	masky[1][0] = -2;
	masky[1][1] = 0;
	masky[1][2] = 2;
	masky[2][0] = -1;
	masky[2][1] = 0;
	masky[2][2] = 1;

	Mat imgPadding;
	convertImagePadding(sourceImage, imgPadding, 1, 3, 3);

	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());
	int  c;
	int gx, gy, gx2, gy2, gx3, gy3;

	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, 3, 3, 2);
				vector<vector<int>> SobelMask = CreateSobelMask();

				if (!neighbor.empty())
				{
					c = 0;
					gx = 0;
					gy = 0;

					for (int ii = 0; ii < nMask; ii++)
					{
						for (int jj = 0; jj < nMask; jj++)
						{
							gx += neighbor[0][c] * maskx[ii][jj];
							gy += neighbor[0][c] * masky[ii][jj];
							c++;
						}
					}

					tmp.at<uchar>(i, j) = saturate_cast<uchar>((sqrt(gx*gx + gy * gy)) / 4);
				}
			}
		}
	}
	break;
	case 3://ảnh màu
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, 3, 3, 2);
				if (!neighbor.empty())
				{
					c = 0;
					gx = gx2 =gx3= 0;
					gy = gy2 = gy3 = 0;

					for (int ii = 0; ii < nMask; ii++)
					{
						for (int jj = 0; jj < nMask; jj++)
						{
							gx += neighbor[0][c] * maskx[ii][jj];
							gy += neighbor[0][c] * masky[ii][jj];

							gx2 += neighbor[1][c] * maskx[ii][jj];
							gy2 += neighbor[1][c] * masky[ii][jj];

							gx3 += neighbor[2][c] * maskx[ii][jj];
							gy3 += neighbor[2][c] * masky[ii][jj];

							c++;
						}
					}

					tmp.at<Vec3b>(i, j)[0] = saturate_cast<uchar>((sqrt(gx*gx + gy * gy)) / 4);
					tmp.at<Vec3b>(i, j)[1] = saturate_cast<uchar>((sqrt(gx2*gx2 + gy2 * gy2)) / 4);
					tmp.at<Vec3b>(i, j)[2] = saturate_cast<uchar>((sqrt(gx3*gx3 + gy3 * gy3)) / 4);

				}
			}
		}
		break;
	}
	default:
		return -1;
		break;
	}
	dropImage(tmp, destinationImage, 3, 3);
	return 0;
}

int laplacianFilter(Mat sourceImage, Mat &destinationImage)
{
	//Tạo mask	3x3 cho laplacian filter
	int **mask;
	mask = new int*[nMask];
	for (int i = 0; i < nMask; i++)
	{
		mask[i] = new int[nMask];
	}

	//Gán tĩnh giá trị cho mask
	mask[0][0] = 1;
	mask[0][1] = 1;
	mask[0][2] = 1;
	mask[1][0] = 1;
	mask[1][1] = -8;
	mask[1][2] = 1;
	mask[2][0] = 1;
	mask[2][1] = 1;
	mask[2][2] = 1;

	Mat imgPadding;
	convertImagePadding(sourceImage, imgPadding, 1, 3, 3);
	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());
	int value,value2,value3,c;
	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, 3, 3, 2);
				if (!neighbor.empty())
				{
					//Giả sử	0	0	0	với mask	1	1	1
					//			0	255	0				1	-8	1
					//			0	0	0				1	1	1
					//giá trị /8 để scale lại về đoạn 0,255

					value = 0;
					c = 0;
					for (int ii = 0; ii < nMask; ii++)
					{
						for (int jj = 0; jj < nMask; jj++)
						{
							value += neighbor[0][c]*mask[ii][jj];
							c++;
						}
					}
					tmp.at<uchar>(i, j) = abs(value) / 8;
				}
			}
		}
	}
	break;
	case 3://ảnh màu
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, 3, 3, 2);
				if (!neighbor.empty())
				{
					//Giả sử	0	0	0	với mask	1	1	1
					//			0	255	0				1	-8	1
					//			0	0	0				1	1	1
					//giá trị /8 để scale lại về đoạn 0,255

					value = 0;
					value2 = 0;
					value3 = 0;
					c = 0;
					for (int ii = 0; ii < nMask; ii++)
					{
						for (int jj = 0; jj < nMask; jj++)
						{
							value += neighbor[0][c] * mask[ii][jj];
							value2 += neighbor[1][c] * mask[ii][jj];
							value3 += neighbor[2][c] * mask[ii][jj];
							c++;
						}
					}

					tmp.at<Vec3b>(i, j)[0] = abs(value) / 8;

					tmp.at<Vec3b>(i, j)[1] = abs(value2) / 8;

					tmp.at<Vec3b>(i, j)[2] = abs(value3) / 8;
				}
			}
		}
	}
	break;
	default:
		return -1;
		break;
	}
	dropImage(tmp, destinationImage, 3, 3);
	return 0;
}

int unsharpMaskingProc(Mat sourceImage, Mat &destinationImage, int k)
{
	gaussianFilter(sourceImage, destinationImage, 3, 3, 1, 1);
	Mat tmp(destinationImage.rows, destinationImage.cols, destinationImage.type());
	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < destinationImage.rows; i++)
		{
			for (int j = 0; j < destinationImage.cols; j++)
			{
				tmp.at<uchar>(i, j) = saturate_cast<uchar>(sourceImage.at<uchar>(i, j) + 
					k * (sourceImage.at<uchar>(i, j) - 
						destinationImage.at<uchar>(i, j)));
			}
		}
	}
	break;
	case 3://ảnh màu
	{

		for (int i = 0; i < destinationImage.rows; i++)
		{
			for (int j = 0; j < destinationImage.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(destinationImage, i, j, 3, 3, 2);
				if (!neighbor.empty())
				{
					if (!neighbor[0].empty())
					{
						int sum1 = 0, sum2 = 0, sum3 = 0;
						for (int k = 0; k < neighbor[0].size(); k++)
						{
							sum1 += neighbor[0][k];
							sum2 += neighbor[1][k];
							sum3 += neighbor[2][k];
						}
						tmp.at<Vec3b>(i, j)[0] = saturate_cast<uchar>(sourceImage.at<Vec3b>(i, j)[0] +
							k * (sourceImage.at<Vec3b>(i, j)[0] -
								destinationImage.at<Vec3b>(i, j)[0]));

						tmp.at<Vec3b>(i, j)[1] = saturate_cast<uchar>(sourceImage.at<Vec3b>(i, j)[1] +
							k * (sourceImage.at<Vec3b>(i, j)[1] -
								destinationImage.at<Vec3b>(i, j)[1]));

						tmp.at<Vec3b>(i, j)[2] = saturate_cast<uchar>(sourceImage.at<Vec3b>(i, j)[2] +
							k * (sourceImage.at<Vec3b>(i, j)[2] -
								destinationImage.at<Vec3b>(i, j)[2]));

					}
				}
			}
		}
	}
	break;
	default:
		return -1;
		break;
	}
	destinationImage = tmp;
	return 0;
}
//Bước tiền xử lí cho bộ lọc Gaussian
vector<double> createGaussianKernel(int w, int h, int sigma)
{
	//Khởi tạo kernel
	vector<double> kernel;

	double r, sumKernel = 0;

	for (int i = -(h / 2); i <= (h / 2); i++)
	{
		for (int j = -(w / 2); j <= (w / 2); j++)
		{
			r = i * i + j * j;
			kernel.push_back(exp(-(r / (2 * sigma*sigma))));
			sumKernel += exp(-(r / (2 * sigma*sigma)));
		}
		cout << endl;

	}
	//	dis(kernel, w, h);
		//sum = sumKernel;
	for (int i = 0; i < kernel.size(); i++)
	{
		kernel[i] /= sumKernel;
	}
	return  kernel;
}

//Bộ lọc Gaussian
int gaussianFilter(Mat sourceImage, Mat & destinationImage, int w, int h, float sigma, int option)
{
	Mat  imgPadding;
	convertImagePadding(sourceImage, imgPadding, option, w, h);
	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());

	vector<double> kernel = createGaussianKernel(w, h, sigma);
	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				double value = 0;
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h, 2);
				if (!neighbor.empty() && !neighbor[0].empty())
				{
					for (int c = 0; c < neighbor[0].size(); c++)
					{
						value += kernel[c] * neighbor[0][c];
					}
				}
				tmp.at<uchar>(i, j) = (int)value;
			}
		}
		dropImage(tmp, destinationImage, w, h);
		return 1;
	}
	break;
	case 3: //Ảnh RGB
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				double value1 = 0, value2 = 0, value3 = 0;
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h, 2);
				if (!neighbor.empty() && !neighbor[0].empty())
				{
					for (int c = 0; c < neighbor[0].size(); c++)
					{
						value1 += kernel[c] * neighbor[0][c];
					}
				}
				if (!neighbor.empty() && !neighbor[1].empty())
				{
					for (int c = 0; c < neighbor[1].size(); c++)
					{
						value2 += kernel[c] * neighbor[1][c];
					}
				}
				if (!neighbor.empty() && !neighbor[2].empty())
				{
					for (int c = 0; c < neighbor[2].size(); c++)
					{
						value3 += kernel[c] * neighbor[2][c];
					}
				}
				tmp.at<Vec3b>(i, j)[2] = (int)value3;
				tmp.at<Vec3b>(i, j)[1] = (int)value2;
				tmp.at<Vec3b>(i, j)[0] = (int)value1;
			}
		}
		dropImage(tmp, destinationImage, w, h);
		return 1;
	}
	break;
	default:
		return -1;
		break;
	}
	return 0;
}
