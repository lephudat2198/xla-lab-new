﻿#pragma once

#include"Libraries/Headers/opencv2/core.hpp"
#include"Libraries/Headers/opencv2/imgproc.hpp"
#include"Libraries/Headers/opencv2/highgui.hpp"


#include<iostream>
using namespace std;
using namespace cv;


//padding ảnh theo  
//w: chiều dài
//h: chiều cao
//Option: 
//1: replating
//0: zero
void convertImagePadding(Mat sourceImage, Mat & destinationImage, int option, int w, int h);


//Lấy các giá trị láng riềng của điểm center
//Trả về mảng 2 chiều các giá trị theo channels 0|1234567....
//w: chiều dài
//h: chiều cao
//option 1 - ascend array
//option 2 - normal array
vector<vector<int>> getNeighbors(Mat sourceImg, int x, int y, int w, int h, int  option);


//Drop ảnh vừa size của ảnh gốc
void dropImage(Mat sourceImage, Mat & destinationImage, int w, int h);


//Bộ lọc Robert cross gradient
vector <vector<int>> CreateRobertMask();
int robertFilter(Mat sourceImage, Mat &destinationImage);


//Bộ lọc Sobel
vector <vector<int>> CreateSobelMask();
int sobelFilter(Mat sourceImage, Mat &destinationImage);


//Bộ lọc Laplacian
int laplacianFilter(Mat sourceImage, Mat &destinationImage);

// Bộ lọc Unsharp Masking
int unsharpMaskingProc(Mat sourceImage, Mat &destinationImage, int k);


//Bộ lọc Gaussian
int gaussianFilter(Mat sourceImage, Mat &destinationImage, int w, int h, float sigma, int option);
vector<double> createGaussianKernel(int w, int h, int sigma);
