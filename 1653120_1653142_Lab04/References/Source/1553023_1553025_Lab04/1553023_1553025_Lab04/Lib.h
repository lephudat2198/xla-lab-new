#pragma once
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
using namespace cv;
using namespace std;

//Ideal Lowpass Filter 
int idealLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int debug);

//ButterworthLowpassFilter
int butterworthLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int n, int debug);

//Gaussian Lowpass Filter
int gaussianLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int debug);