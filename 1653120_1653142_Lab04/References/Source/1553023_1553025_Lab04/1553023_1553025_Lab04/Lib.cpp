﻿#include"Lib.h"

void Padding(Mat src, Mat& dst)
{
	dst.create(2 * src.rows,2 * src.cols, src.type()); // tao ma tran mới với kích thước đã padding

	for (int i = 0; i < dst.rows; i++)  // chép ảnh nguồn vào ảnh mới với vị trí thích hợp
	{
		for (int j = 0; j < dst.cols; j++)
		{
			if (i >= 0 && i < src.rows && j >= 0 && j < src.cols) // copy những pixel trong ảnh gốc
			{
				dst.at<uchar>(i, j) = src.at<uchar>(i, j);
			}
			else // vùng padding cho các giá trị = 0
			{
				dst.at<uchar>(i, j) = 0;
			}
		}
	}
}

void Multiply(Mat& dst)
{

	for (int i = 0; i < dst.rows / 2; i++)
	{
		for (int j = 0; j < dst.cols / 2; j++)
		{
			dst.at<uchar>(i, j) = saturate_cast<uchar>(dst.at<uchar>(i, j) * pow(-1, i + j)); // nhân pixel cho (-1)^(x + y)
		}
	}
}

void swapDFT(Mat& fImage)
{
	Mat tmp, q0, q1, q2, q3;

	// fix lại kích thước của ảnh nếu M, N là số lẻ
	fImage = fImage(Rect(0, 0, fImage.cols & -2, fImage.rows & -2));

	int x = fImage.cols / 2;
	int y = fImage.rows / 2;

	// Sắp xếp lại tọa độ của ảnh quang phổ để quang phổ nằm vào trung tâm ảnh.

	q0 = fImage(Rect(0, 0, x, y)); // góc phần tư thứ nhất
	q1 = fImage(Rect(x, 0, x, y)); // góc phần tư thứ 2
	q2 = fImage(Rect(0, y, x, y)); // góc phần tư thứ 4
	q3 = fImage(Rect(x, y, x, y)); //góc phần tư thứ 3

	q0.copyTo(tmp); // hoán vị góc phần tư thứ nhất và thứ 3
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp); // hoán vị góc phần tư thứ 2 và thứ 4
	q2.copyTo(q1);
	tmp.copyTo(q2);
}

void Mymagnitude(Mat input1, Mat input2, Mat& output)
{
	for (int i = 0; i < input1.rows; i++)
	{
		for (int j = 0; j < input1.cols; j++)
		{
			float x, y;
			x = input1.at<float>(i, j);
			y = input2.at<float>(i, j);
			output.at<float>(i, j) = sqrt(x*x + y*y);
		}
	}
}

void Mylog(Mat input, Mat& ouput)
{
	for (int i = 0; i < input.rows; i++)
	{
		for (int j = 0; j < input.cols; j++)
		{
			ouput.at<float>(i, j) = log(input.at<float>(i, j));
		}
	}
}

void Mysplit(Mat input, Mat* output)
{

	for (int c = 0; c < input.channels(); c++)
	{
		Mat tmp(input.size(), CV_32F);
		for (int i = 0; i < input.rows; i++)
		{
			for (int j = 0; j < input.cols; j++)
			{
				tmp.at<float>(i, j) = input.at<Vec2f>(i, j)[c];
			}
		}
		tmp.copyTo(output[c]);
	}
}

void Mymerge(Mat* input, int count, Mat& output)
{
	Mat tmp(input[0].size(), CV_32FC2);
	output = tmp.clone();
	for (int c = 0; c < count; c++)
	{

		for (int i = 0; i < tmp.rows; i++)
		{
			for (int j = 0; j < tmp.cols; j++)
			{
				output.at<Vec2f>(i, j)[c] = input[c].at<float>(i, j);
			}
		}

	}

}

Mat Spectrum(Mat& complex, bool check)
{
	Mat planes[2];

	Mysplit(complex, planes); // chia complex thành hai phần riêng lẽ
	Mymagnitude(planes[0], planes[1], planes[0]); // tính độ lớn của hai phần vừa chia

	Mat mag = (planes[0]).clone();
	mag += Scalar::all(1);
	Mylog(mag, mag); // lấy log cho mag

	if (check)
		swapDFT(mag); // sắp xếp lại các gốc phần tư của ảnh

	normalize(mag, mag, 0, 1, CV_MINMAX);

	return mag;

}

void CreatePaddingMask(Mat src, Mat *planes)
{
	Mat padImg;
	Padding(src, padImg); // padding vào ảnh với kích thước P = 2M, Q = 2N và lưu ảnh padding vào padImg
	Multiply(padImg); // nhân ảnh padding vừa tạo với (-1)^(x + y) -> theo bước thứ 3

	planes[0] = Mat_<float>(padImg); // xây dựng matrix dựa trên padImg
	planes[1] = Mat::zeros(padImg.size(), CV_32F); // xây dựng ma trận zeros với type = CV_32F, kích thước là PxQ
}

void ApplyFilter(Mat &complex, Mat filter, Mat *planes, Mat &imgOutput, Mat &mag)
{
	swapDFT(complex); // hoán vị các góc phần tư
	mulSpectrums(complex, filter, complex, 0); // nhân quang phổ complex = complex * filter
	swapDFT(complex); // hoán vị trở lại ban đầu

	mag = Spectrum(complex,true); // tạo quang phổ và gán cho mag
	idft(complex, complex); // tạo IDFT trong ảnh đã lọc

	Mysplit(complex, planes); // chia complex thành 2 kênh riêng lẽ như ảnh ban đầu
	normalize(planes[0], imgOutput, 0, 1, CV_MINMAX);

	Mysplit(filter, planes); // chia complex thành 2 kênh riêng lẽ như ảnh đã lọc
}

Mat CropImage(Mat dst) // cắt ảnh output sau khi hoàn thành
{
	Mat tmp = Mat(dst.rows / 2, dst.cols / 2, dst.type());
	for (int i = 0; i < dst.rows / 2; i++)
		for (int j = 0; j < dst.cols / 2; j++)
			tmp.at<float>(i, j) = dst.at<float>(i, j);

	return tmp;
}

void ILPF(Mat &Filter, int Dzero)
{
	Mat tmp = Mat(Filter.rows, Filter.cols, CV_32F);

	// tọa độ trung tâm x, y của quang phổ
	int x = Filter.rows / 2;
	int y = Filter.cols / 2;
	double D;

	for (int i = 0; i < Filter.rows; i++)
	{
		for (int j = 0; j < Filter.cols; j++)
		{
			D = (double)sqrt(pow((i - x), 2.0) + pow((double)(j - y), 2.0)); // D(u, v)
																			 //Áp dụng công thức của Ideal Lowpass Filter
			if (D <= Dzero)
				tmp.at<float>(i, j) = 1;
			else
				tmp.at<float>(i, j) = 0;
		}
	}

	Mat toMerge[] = { tmp, tmp };
	Mymerge(toMerge, 2, Filter); // kết hợp 2 tmp tạo bộ lọc 2D
}

void BLPF(Mat &Filter, int Dzero, int n)
{
	Mat tmp = Mat(Filter.rows, Filter.cols, CV_32F);

	// tọa độ trung tâm x, y của quang phổ
	int x = Filter.rows / 2;
	int y = Filter.cols / 2;
	double D;

	for (int i = 0; i < Filter.rows; i++)
	{
		for (int j = 0; j < Filter.cols; j++)
		{
			D = (double)sqrt(pow((i - x), 2.0) + pow((double)(j - y), 2.0)); // D(u, v)
																			 // Áp dụng công thức của Butterworth Lowpass Filter
			tmp.at<float>(i, j) = (float)(1 / (1 + pow((double)(D / Dzero), (double)(2 * n))));
		}
	}

	Mat toMerge[] = { tmp, tmp };
	Mymerge(toMerge, 2, Filter); // kết hợp 2 tmp tạo bộ lọc 2D
}

void GLPF(Mat &Filter, int Dzero)
{
	Mat tmp = Mat(Filter.rows, Filter.cols, CV_32F);

	// tọa độ trung tâm x, y của quang phổ
	int x = Filter.rows / 2;
	int y = Filter.cols / 2;
	double D;

	for (int i = 0; i < Filter.rows; i++)
	{
		for (int j = 0; j < Filter.cols; j++)
		{
			D = (double)sqrt(pow((i - x), 2.0) + pow((double)(j - y), 2.0)); // D(u, v)
																			 // Áp dụng công thức của Gaussian Lowpass Filter
			tmp.at<float>(i, j) = exp((-1.0) * (D*D) / (float)(2 * Dzero * Dzero));
		}
	}

	Mat toMerge[] = { tmp, tmp };
	Mymerge(toMerge, 2, Filter); // kết hợp 2 tmp tạo bộ lọc 2D
}

int idealLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int debug)
{
	Mat after, before, filter, *planes, complex, tmp;
	//Tạo padding có kích thước P = 2M và Q = 2N, ảnh gốc nằm ở vị trí từ (0, 0) đến (M, N)
	planes = new Mat[2];
	CreatePaddingMask(sourceImage, planes);

	//Tinh dft
	merge(planes, 2, complex); // kết hợp hai kênh trong mảng planes với nhau.
	dft(complex, complex); // tính dft cho kênh màu vừa merge

	//xuất ra quang phổ trước khi áp bộ lọc vào
	Mat copy;
	complex.copyTo(copy);
	before = Spectrum(copy, true);

	//Xay dung bo loc
	filter = complex.clone();
	ILPF(filter, threshold);

	//Apply bộ lọc vào ảnh padding vừa tạo và xuất ra các kết quả quang phổ trước và sau convolution, ảnh Output
	ApplyFilter(complex, filter, planes, tmp, after);
	destinationImage = CropImage(tmp);

	if (debug == 1)
	{
		imshow("Spectrum Fourier Before", before);
		imshow("Spectrum Fourie After", after);
	}
	return 1;
}

int butterworthLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int n, int debug)
{
	Mat after, before, filter, *planes, complex, tmp;
	//Tạo padding có kích thước P = 2M và Q = 2N, ảnh gốc nằm ở vị trí từ (0, 0) đến (M, N)
	planes = new Mat[2];
	CreatePaddingMask(sourceImage, planes);

	//Tinh dft
	Mymerge(planes, 2, complex); // kết hợp hai kênh trong mảng planes với nhau.
	dft(complex, complex); // tính dft cho kênh màu vừa merge

	//xuất ra quang phổ trước khi áp bộ lọc vào
	Mat copy;
	complex.copyTo(copy);
	before = Spectrum(copy, true);

	//Xay dung bo loc
	filter = complex.clone();
	BLPF(filter, threshold, n);

	//Apply bộ lọc vào ảnh padding vừa tạo và xuất ra các kết quả quang phổ trước và sau convolution, ảnh Output
	ApplyFilter(complex, filter, planes, tmp, after);
	destinationImage = CropImage(tmp);

	if (debug == 1)
	{
		imshow("Spectrum Fourier Before", before);
		imshow("Spectrum Fourie After", after);
	}
	return 1;
}

int gaussianLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int debug)
{
	Mat after, before, filter, *planes, complex, tmp;
	//Tạo padding có kích thước P = 2M và Q = 2N, ảnh gốc nằm ở vị trí từ (0, 0) đến (M, N)
	planes = new Mat[2];
	CreatePaddingMask(sourceImage, planes);

	//Tinh dft
	Mymerge(planes, 2, complex); // kết hợp hai kênh trong mảng planes với nhau.
	dft(complex, complex); // tính dft cho kênh màu vừa merge

	//xuất ra quang phổ trước khi áp bộ lọc vào
	Mat copy;
	complex.copyTo(copy);
	before = Spectrum(copy, true);

	//Xay dung bo loc
	filter = complex.clone();
	GLPF(filter, threshold);

	//Apply bộ lọc vào ảnh padding vừa tạo và xuất ra các kết quả quang phổ trước và sau convolution, ảnh Output
	ApplyFilter(complex, filter, planes, tmp, after);
	destinationImage = CropImage(tmp);

	if (debug == 1)
	{
		imshow("Spectrum Fourier Before", before);
		imshow("Spectrum Fourie After", after);
	}
	return 1;
}
