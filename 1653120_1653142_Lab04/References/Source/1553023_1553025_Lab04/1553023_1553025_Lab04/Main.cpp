﻿#include"Lib.h"

void main(int argc, char *argv[])
{
	Mat src, dst;
	src = imread(argv[1], IMREAD_GRAYSCALE);
	if (src.empty())
	{
		cout << "Could not find your image!!!\n";
		return;
	}
	switch (atoi(argv[2]))
	{
		case 1:
		{
			idealLowpass(src, dst, atoi(argv[3]), atoi(argv[4]));
			imshow("Orginal image", src);
			imshow("Result", dst);
		}
		break;

		case 2:
		{
			butterworthLowpass(src, dst, atoi(argv[3]), atoi(argv[4]), atoi(argv[5]));
			imshow("Orginal image", src);
			imshow("Result", dst);
		}
		break;

		case 3:
		{
			gaussianLowpass(src, dst, atoi(argv[3]), atoi(argv[4]));
			imshow("Orginal image", src);
			imshow("Result", dst);
		}
		break;

		default:
			cout << "Your input is wrong!\n";
			break;
	}
	waitKey(0);
}
