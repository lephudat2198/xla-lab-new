﻿#pragma once

#include"Libraries/Headers/opencv2/core.hpp"
#include"Libraries/Headers/opencv2/imgproc.hpp"
#include"Libraries/Headers/opencv2/highgui.hpp"

#include<iostream>

using namespace std;
using namespace cv;

//Bộ lọc Ideal
//Lowpass
//Trong đó :
//-threshold : giá trị ngưỡng D0
//- debug : 0 – Không hiển thị phổ
//Fourier, 1 – hiển thị phổ Fourier
//trước và sau khi thực hiện
//convolution.

int idealLowpass(Mat sourceImage,
	Mat &destinationImage, int threshold,
	int debug);

//2 Bộ lọc Butterworth
//Lowpass
//
//Trong đó :
////-threshold, debug : như trên
//- n : bậc của bộ lọc
int butterworthLowpass(Mat
	sourceImage, Mat &destinationImage,
	int threshold, int n, int debug);

//3 Bộ lọc Gaussian
//Lowpass
//Trong đó :
//-threshold, debug : như trên
int gaussianLowpass(Mat
	sourceImage, Mat &destinationImage,
	int threshold, int debug);

void displayImageValue(Mat src);

//Image padding - Parameters P = 2M và Q = 2N
void PaddingImage(Mat src, Mat& dst);

//Image multiply to center its transform.
//Input: padded image
//Output: Multiply (-1)^(x+y) image
void MultiplyImage(Mat src, Mat & dst);

//Gộp 2 ảnh CV_32FC1 vào 1 ảnh CV_32FC2
void MergeImage(Mat* planes, int numofplanes, Mat &dst);

//Chia 1 ảnh CV_32FC2 ra 2 ảnh CV_32FC1
void SplitImage(Mat src, Mat* planes);

//Tính modun theo số phức
void MagnitudeImage(Mat plane1, Mat plane2, Mat &dst);

//
void LogImage(Mat src, Mat& dst);

//Chuyển ra giữa 
void CenterImage(Mat &src);

void FourierTransform(Mat src, Mat &dst);

void FourierTransformIdeal(Mat src, Mat &dst, int threshold, Mat &after);

void IdealLP(Mat &src, int threshold);

void ButterworthLP(Mat &src, int threshold, int n);

void FourierTransformButter(Mat src, Mat &dst, int threshold, Mat &after, int n);

void GaussianLP(Mat &src, int threshold);

void FourierTransformGaussian(Mat src, Mat &dst, int threshold, Mat &after);