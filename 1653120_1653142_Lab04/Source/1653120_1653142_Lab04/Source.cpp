﻿#include "Header.h"


//Bộ lọc Ideal
//Lowpass
//Trong đó :
//-threshold : giá trị ngưỡng D0
//- debug : 0 – Không hiển thị phổ
//Fourier, 1 – hiển thị phổ Fourier
//trước và sau khi thực hiện
//convolution.
int idealLowpass(Mat sourceImage,
	Mat &destinationImage, int threshold,
	int debug)
{
	Mat before, after;
	FourierTransform(sourceImage, before);
	FourierTransformIdeal(sourceImage, destinationImage, threshold, after);
	if (debug == 1)
	{
		namedWindow("BefImage", WINDOW_AUTOSIZE);
		imshow("BefImage", before);
		namedWindow("AftImage", WINDOW_AUTOSIZE);
		imshow("AftImage", after);
	}
	return 0;
}

//2 Bộ lọc Butterworth
//Lowpass
//
//Trong đó :
//-threshold, debug : như trên
//- n : bậc của bộ lọc
int butterworthLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int n, int debug)
{
	Mat before, after;
	FourierTransform(sourceImage, before);
	FourierTransformButter(sourceImage, destinationImage, threshold, after, n);
	if (debug == 1)
	{
		namedWindow("BefImage", WINDOW_AUTOSIZE);
		imshow("BefImage", before);
		namedWindow("AftImage", WINDOW_AUTOSIZE);
		imshow("AftImage", after);
	}
	return 0;
}

//3 Bộ lọc Gaussian
//Lowpass
//Trong đó :
//-threshold, debug : như trên
int gaussianLowpass(Mat sourceImage, Mat &destinationImage, int threshold, int debug)
{
	Mat before, after;
	FourierTransform(sourceImage, before);
	FourierTransformGaussian(sourceImage, destinationImage, threshold, after);
	if (debug == 1)
	{
		namedWindow("BefImage", WINDOW_AUTOSIZE);
		imshow("BefImage", before);
		namedWindow("AftImage", WINDOW_AUTOSIZE);
		imshow("AftImage", after);
	}
	return 0;
}

void displayImageValue(Mat src)
{
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			cout << (float)src.at<uchar>(i, j) << " ";
		}
		cout << endl;
	}
}

//Image padding - Parameters P = 2M và Q = 2N
void PaddingImage(Mat src, Mat& dst)
{
	if (src.empty() || src.type() != CV_8UC1)
	{
		cout << "Anh dau vao khong hop le\n";
		return;
	}

	Mat imgPadded(src.rows * 2, src.cols * 2, src.type(), Scalar(0));
	//Chỉ chép 1 phần tư bên trái cùng, còn lại = 0
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			imgPadded.at<uchar>(i, j) =
				saturate_cast<uchar>(src.at<uchar>(i, j));
		}
	}
	dst = imgPadded;
	//displayImageValue(imgPadded);
}



//Image multiply to center its transform.
//Input: padded image
//Output: Multiply (-1)^(x+y) image
void MultiplyImage(Mat src, Mat & dst)
{
	Mat imgPadded(src.rows, src.cols, src.type(), Scalar(0));

	for (int i = 0; i < src.rows / 2; i++)
	{
		for (int j = 0; j < src.cols / 2; j++)
		{
			imgPadded.at<uchar>(i, j) =
				saturate_cast<uchar>(src.at<uchar>(i, j)*pow(-1, i + j));
		}
	}
	dst = imgPadded;
	//displayImageValue(imgPadded);
}

void MergeImage(Mat* planes, int numofplanes, Mat &dst)
{
	Mat tmp(planes[0].size(), CV_32FC2);
	for (int i = 0; i < planes[0].rows; i++)
	{
		for (int j = 0; j < planes[0].cols; j++)
		{
			for (int c = 0; c < numofplanes; c++)
			{
				tmp.at<Vec2f>(i, j)[c] = planes[c].at<float>(i, j);
			}
		}
	}
	dst = tmp.clone();
}

void SplitImage(Mat src, Mat* planes)
{
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			for (int c = 0; c < src.channels(); c++)
			{
				planes[c].at<float>(i, j) = src.at<Vec2f>(i, j)[c];
			}
		}
	}
}

void MagnitudeImage(Mat plane1, Mat plane2, Mat &dst)
{
	float Re, Im;
	for (int i = 0; i < plane1.rows; i++)
	{
		for (int j = 0; j < plane1.cols; j++)
		{
			Re = plane1.at<float>(i, j);
			Im = plane2.at<float>(i, j);
			dst.at<float>(i, j) = sqrt(Re*Re + Im * Im);
		}
	}
}

void LogImage(Mat src, Mat& dst)
{
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			dst.at<float>(i, j) = log(src.at<float>(i, j));
		}
	}
}

void CenterImage(Mat &src)
{
	src = src(Rect(0, 0, src.cols & -2, src.rows & -2));
	int cx = src.cols / 2;
	int cy = src.rows / 2;
	Mat q0(src, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	Mat q1(src, Rect(cx, 0, cx, cy));  // Top-Right
	Mat q2(src, Rect(0, cy, cx, cy));  // Bottom-Left
	Mat q3(src, Rect(cx, cy, cx, cy)); // Bottom-Right

	Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	normalize(src, src, 0, 1, CV_MINMAX);
}

//Cắt phần padding của ảnh
//Output là ảnh cùng size với ảnh gốc
void CroppingImage(Mat src, Mat &dst) // cắt ảnh output sau khi hoàn thành
{
	Mat imgCropped(src.rows / 2, src.cols / 2, src.type());
	for (int i = 0; i < src.rows / 2; i++)
	{
		for (int j = 0; j < src.cols / 2; j++)
		{
			imgCropped.at<float>(i, j) = src.at<float>(i, j);
		}
	}
	dst = imgCropped;
}

//Biến đổi Fourier 
void FourierTransform(Mat src, Mat &dst)
{
	PaddingImage(src, src);//padding ảnh
	MultiplyImage(src, src);//nhân ảnh theo công thức step 3

	//Tạo ra 2 ảnh, 1 theo ảnh vừa padded(phàn thực), 1 zero(phần ảo)
	Mat planes[] = { Mat_<float>(src), Mat::zeros(src.size(), CV_32F) };
	
	Mat tmp;
	//Gộp 2 ảnh về 1 ảnh CV_32FC2
	MergeImage(planes, 2, tmp);
	//Áp dụng Discrete Fourier transform
	dft(tmp, tmp);
	//Cắt ra 2 ảnh CV_32FC1
	SplitImage(tmp, planes);
	//Tính magnitude dựa vào 2 phần thực ảo và sau khi đó lưu vào phần thực
	MagnitudeImage(planes[0], planes[1], planes[0]);
	dst = planes[0];
	//Cộng thêm 1 đơn vị vào ảnh phòng ngừa log(0) bị lỗi
	dst += Scalar::all(1);
	LogImage(dst, dst);
	CenterImage(dst);
}

//Ideal lowpass filter
//threshold
void IdealLP(Mat &src, int threshold)
{
	Mat tmp = Mat(src.rows, src.cols, CV_32F);
	float D;
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			D = sqrt((i - src.rows / 2)*(i - src.rows / 2) + (j - src.cols / 2)*(j - src.cols / 2)); // D(u, v)
																			 //Áp dụng công thức của Ideal Lowpass Filter
			if (D <= threshold)
				tmp.at<float>(i, j) = 1;
			else
				tmp.at<float>(i, j) = 0;
		}
	}
	Mat planes[] = { tmp,tmp };
	MergeImage(planes, 2, src);
}

void FourierTransformIdeal(Mat src, Mat &dst, int threshold, Mat &after)
{
	//lấy ảnh padded
	PaddingImage(src, src);
	//nhân ảnh
	MultiplyImage(src, src);
	//tạo ra 2 ảnh ảo thực
	Mat planes[] = { Mat_<float>(src), Mat::zeros(src.size(), CV_32F) };
	Mat tmp;
	//gộp 2 ảnh trên thành ảnh 2 kênh màu
	MergeImage(planes, 2, tmp);

	dft(tmp, tmp);
	Mat filter = tmp.clone();

	IdealLP(filter, threshold);
	CenterImage(filter);
	mulSpectrums(tmp, filter, tmp, 0);
	SplitImage(tmp, planes);
	MagnitudeImage(planes[0], planes[1], planes[0]);
	dst = planes[0];
	dst += Scalar::all(1);
	LogImage(dst, dst);
	CenterImage(dst);
	after = dst.clone();
	idft(tmp, tmp);
	SplitImage(tmp, planes);
	normalize(planes[0], tmp, 0, 1, CV_MINMAX);
	//Cắt phần padding
	CroppingImage(tmp, dst);
}

void ButterworthLP(Mat &src, int threshold, int n)
{
	Mat tmp = Mat(src.rows, src.cols, CV_32F);
	float D;
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			D = sqrt((i - src.rows / 2)*(i - src.rows / 2) + (j - src.cols / 2)*(j - src.cols / 2)); // D(u, v)
																			 //Áp dụng công thức của Butterworth Lowpass Filter
			tmp.at<float>(i, j) = 1 / (1 + pow((D / threshold), 2 * n));
		}
	}
	Mat planes[] = { tmp,tmp };
	MergeImage(planes, 2, src);
}

void FourierTransformButter(Mat src, Mat &dst, int threshold, Mat &after, int n)
{
	PaddingImage(src, src);
	MultiplyImage(src, src);
	Mat planes[] = { Mat_<float>(src), Mat::zeros(src.size(), CV_32F) };
	Mat tmp;
	MergeImage(planes, 2, tmp);
	dft(tmp, tmp);
	Mat filter = tmp.clone();
	ButterworthLP(filter, threshold, n);
	CenterImage(filter);
	mulSpectrums(tmp, filter, tmp, 0);
	SplitImage(tmp, planes);
	MagnitudeImage(planes[0], planes[1], planes[0]);
	dst = planes[0];
	dst += Scalar::all(1);
	LogImage(dst, dst);
	CenterImage(dst);
	after = dst.clone();
	idft(tmp, tmp);
	SplitImage(tmp, planes);
	normalize(planes[0], tmp, 0, 1, CV_MINMAX);
	CroppingImage(tmp, dst);
}

void GaussianLP(Mat &src, int threshold)
{
	Mat tmp = Mat(src.rows, src.cols, CV_32F);
	float D;
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			D = sqrt((i - src.rows / 2)*(i - src.rows / 2) + (j - src.cols / 2)*(j - src.cols / 2)); // D(u, v)
																			 //Áp dụng công thức của Gaussian Lowpass Filter
			tmp.at<float>(i, j) = exp((-1.0) * D * D / (2 * threshold * threshold));
		}
	}
	Mat planes[] = { tmp,tmp };
	MergeImage(planes, 2, src);
}

void FourierTransformGaussian(Mat src, Mat &dst, int threshold, Mat &after)
{
	PaddingImage(src, src);
	MultiplyImage(src, src);
	Mat planes[] = { Mat_<float>(src), Mat::zeros(src.size(), CV_32F) };
	Mat tmp;
	MergeImage(planes, 2, tmp);
	dft(tmp, tmp);
	Mat filter = tmp.clone();
	GaussianLP(filter, threshold);
	CenterImage(filter);
	mulSpectrums(tmp, filter, tmp, 0);
	SplitImage(tmp, planes);
	MagnitudeImage(planes[0], planes[1], planes[0]);
	dst = planes[0];
	dst += Scalar::all(1);
	LogImage(dst, dst);
	CenterImage(dst);
	after = dst.clone();
	idft(tmp, tmp);
	SplitImage(tmp, planes);
	normalize(planes[0], tmp, 0, 1, CV_MINMAX);
	CroppingImage(tmp, dst);
}
