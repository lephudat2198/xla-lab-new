﻿#include"Header.h"

void main(int argc, char *argv[])
{
	if (argc < 3)
	{
		cout << "Tham so khong hop le\n";
		system("pause");
		return;
	}
	Mat srcImg, dstImg;
	srcImg = imread(argv[1], IMREAD_GRAYSCALE);

	if (strcmp(argv[2], "1") == 0)
	{
		if (argc != 5)
		{
			cout << "Tham so khong phu hop, ideal Low-pass yeu cau 5 object\n"<<endl;
			return;
		}
		idealLowpass(srcImg, dstImg, atoi(argv[3]), atoi(argv[4]));
	}
	else if (strcmp(argv[2], "2") == 0)
	{
		if (argc != 6)
		{
			cout << "Tham so khong phu hop, butterworth Low-pass yeu cau 6 object\n" << endl;
			return;
		}
		butterworthLowpass(srcImg, dstImg, atoi(argv[3]),atoi(argv[4]), atoi(argv[5]));
	}
	else if (strcmp(argv[2], "3") == 0)
	{
		if (argc != 5)
		{
			cout << "Tham so khong phu hop, gaussian Low-pass yeu cau 5 object\n" << endl;
			return;
		}
		gaussianLowpass(srcImg, dstImg, atoi(argv[3]), atoi(argv[4]));
	}
	namedWindow("SrcImage", WINDOW_AUTOSIZE);
	imshow("SrcImage", srcImg);
	namedWindow("DstImage", WINDOW_AUTOSIZE);
	imshow("DstImage", dstImg);
	waitKey(0);
}