﻿#pragma once

#include"Libraries/Headers/opencv2/core.hpp"
#include"Libraries/Headers/opencv2/imgproc.hpp"
#include"Libraries/Headers/opencv2/highgui.hpp"


#include<iostream>
using namespace std;
using namespace cv;


//Bộ lọc trung bình
//Trong đó : -w, h : chiều rộng và chiều cao kernel(số lẻ) - option : 
//0	– sử dụng zero padding, 
//1 	– sử dụng replicating.
int averageFilter(Mat sourceImage,	Mat &destinationImage, int w, int h, int option); 

//Bộ lọc Gaussian 
// Trong đó : -
//w, h, option : như trên - sigma : độ
//lệch chuẩn
int gaussianFilter(Mat sourceImage,	Mat &destinationImage, int w, int h, float sigma, int option);

//Bộ lọc thống kê thứ tự 
//Trong đó : -w, h, option : như trên -
//name : 1 – median filter, 2 – max
//	filter, 3 – min filter
int orderStatisticFilter(Mat sourceImage, Mat &destinationImage, int w, int h, int name, int option);

//padding ảnh theo  
//w: chiều dài
//h: chiều cao
 void convertImagePadding(Mat sourceImage, Mat &destinationImage, int option, int w, int h);

//Lấy các giá trị láng riềng của điểm center
//Trả về mảng 2 chiều các giá trị theo channels 0|1234567....
//w: chiều dài
//h: chiều cao
//option 1 - ascend array
//option 2 - normal array
 vector<vector<int>> getNeighbors(Mat sourceImg,int x, int y, int w, int h, int  option);

 //Drop ảnh vừa size của ảnh gốc
 void dropImage(Mat sourceImage, Mat & destinationImage, int w, int h);
