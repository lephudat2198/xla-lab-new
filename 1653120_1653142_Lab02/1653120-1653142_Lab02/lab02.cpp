﻿#include "lab02.h"


int averageFilter(Mat sourceImage, Mat & destinationImage, int w, int h, int option)
{
	////Concept:
	//	Truyền vào một ảnh.
	//	Chuyển ảnh đó sang dạng padding.
	//	Xử lí trên ảnh padding đó.
	//	Drop ảnh và truyền output ra ngoài.
	
	//Lưu ảnh đã padding
	Mat imgPadding;
	
	convertImagePadding(sourceImage, imgPadding, option, w, h);

	//Copy tmp với số rows,cols,type
	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());

	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,2);
				if (!neighbor.empty())
				{
					int sum = 0;
					for (int k = 0; k < neighbor[0].size(); k++)
					{
						sum += neighbor[0][k];
					}
					tmp.at<uchar>(i, j) = sum / neighbor[0].size();
				}
			}
		}
	}
	break;
	case 3://ảnh màu
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,2);
				if (!neighbor.empty())
				{
					if (!neighbor[0].empty())
					{
						int sum1 = 0, sum2 = 0, sum3 = 0;
						for (int k = 0; k < neighbor[0].size(); k++)
						{
							sum1 += neighbor[0][k];
							sum2 += neighbor[1][k];
							sum3 += neighbor[2][k];
						}
						tmp.at<Vec3b>(i, j)[0] = sum1 / neighbor[0].size();
						tmp.at<Vec3b>(i, j)[1] = sum2 / neighbor[0].size();
						tmp.at<Vec3b>(i, j)[2] = sum3 / neighbor[0].size();
					}
				}
			}
		}
	}
	break;
	default:
		return -1;
		break;
	} 
	//Cắt ảnh theo định size ban đầu
	dropImage(tmp, destinationImage, w, h);
	return 0;
}

vector<double> createGaussianKernel(int w, int h, int sigma)
{
	//Khởi tạo kernel
	vector<double> kernel;
	
	double r, sumKernel =0;

	for (int i = -(h / 2); i <= (h / 2); i++)
	{
		for (int j = -(w / 2); j <= (w / 2); j++)
		{
			r = i * i + j * j;
			kernel.push_back(exp(-(r / (2 * sigma*sigma))));
			sumKernel += exp(-(r / (2 * sigma*sigma)));
		}
		cout << endl;

	}
//	dis(kernel, w, h);
	//sum = sumKernel;
	for (int i = 0; i < kernel.size(); i++)
	{
		kernel[i] /= sumKernel;
	}
	return  kernel;
}

int gaussianFilter(Mat sourceImage, Mat & destinationImage, int w, int h, float sigma, int option)
{
	Mat  imgPadding;
	convertImagePadding(sourceImage, imgPadding, option, w, h);
	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());

	vector<double> kernel = createGaussianKernel(w, h, sigma);
	switch (tmp.channels())
	{
	case 1://Ảnh xám
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				double value = 0;
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h, 2);
				if (!neighbor.empty() && !neighbor[0].empty())
				{
					for (int c = 0; c < neighbor[0].size(); c++)
					{
						value += kernel[c] * neighbor[0][c];
					}
				}
				tmp.at<uchar>(i, j) = (int)value;
			}
		}
		dropImage(tmp, destinationImage, w, h);
		return 1;
	}
	break;
	case 3: //Ảnh RGB
	{
		for (int i = 0; i < imgPadding.rows; i++)
		{
			for (int j = 0; j < imgPadding.cols; j++)
			{
				double value1 = 0, value2 = 0, value3 = 0;
				vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h, 2);
				if (!neighbor.empty() && !neighbor[0].empty())
				{
					for (int c = 0; c < neighbor[0].size(); c++)
					{
						value1 += kernel[c] * neighbor[0][c];
					}
				}
				if (!neighbor.empty() && !neighbor[1].empty())
				{
					for (int c = 0; c < neighbor[1].size(); c++)
					{
						value2 += kernel[c] * neighbor[1][c];
					}
				}
				if (!neighbor.empty() && !neighbor[2].empty())
				{
					for (int c = 0; c < neighbor[2].size(); c++)
					{
						value3 += kernel[c] * neighbor[2][c];
					}
				}
				tmp.at<Vec3b>(i, j)[2] = (int)value3;
				tmp.at<Vec3b>(i, j)[1] = (int)value2;
				tmp.at<Vec3b>(i, j)[0] = (int)value1;
			}
		}
		dropImage(tmp, destinationImage, w, h);
		return 1;
	}
	break;
	default:
		return -1;
		break;
	}
	return 0;
}

int orderStatisticFilter(Mat sourceImage, Mat & destinationImage, int w, int h, int name, int option)
{
	//Lưu ảnh đã padding
	Mat imgPadding;

	convertImagePadding(sourceImage, imgPadding, option, w, h);

	Mat tmp(imgPadding.rows, imgPadding.cols, imgPadding.type());

	//Copy tmp với số rows,cols,type

	switch (name)
	{
	case 1://Median filter
	{
		switch (tmp.channels())
		{
		case 1://Ảnh xám
		{
			for (int i = 0; i < imgPadding.rows; i++)
			{
				for (int j = 0; j < imgPadding.cols; j++)
				{
					vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,1);
					if (!neighbor.empty())
					{						
						tmp.at<uchar>(i, j) = neighbor[0][(w*h) / 2];
					}
				}
			}
		}
		break;
		case 3://ảnh màu
		{
			for (int i = 0; i < imgPadding.rows; i++)
			{
				for (int j = 0; j < imgPadding.cols; j++)
				{
					vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,1);
					if (!neighbor.empty())
					{						
						tmp.at<Vec3b>(i, j)[0] = neighbor[0][(w*h)/2];
						tmp.at<Vec3b>(i, j)[1] = neighbor[1][(w*h) / 2];
						tmp.at<Vec3b>(i, j)[2] = neighbor[2][(w*h) / 2];
					}
				}
			}
		}
		break;
		default:
			return -1;
			break;
		}
		//Cắt ảnh theo định size ban đầu
		dropImage(tmp, destinationImage, w, h);
	}
	break;
	case 2://Max filter
	{
		switch (tmp.channels())
		{
		case 1://Ảnh xám
		{
			for (int i = 0; i < imgPadding.rows; i++)
			{
				for (int j = 0; j < imgPadding.cols; j++)
				{
					vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,1);
					if (!neighbor.empty())
					{						
						tmp.at<uchar>(i, j) = neighbor[0][w*h-1];
					}
				}
			}
		}
		break;
		case 3://ảnh màu
		{
			for (int i = 0; i < imgPadding.rows; i++)
			{
				for (int j = 0; j < imgPadding.cols; j++)
				{
					vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,1);
					if (!neighbor.empty())
					{						
						tmp.at<Vec3b>(i, j)[0] = neighbor[0][w*h - 1];
						tmp.at<Vec3b>(i, j)[1] = neighbor[1][w*h - 1];
						tmp.at<Vec3b>(i, j)[2] = neighbor[2][w*h - 1];
					}
				}
			}
		}
		break;
		default:
			return -1;
			break;
		}
		//Cắt ảnh theo định size ban đầu
		dropImage(tmp, destinationImage, w, h);
		return 1;
	}
	break;
	case 3://Min filter
	{
		switch (tmp.channels())
		{
		case 1://Ảnh xám
		{
			for (int i = 0; i < imgPadding.rows; i++)
			{
				for (int j = 0; j < imgPadding.cols; j++)
				{
					vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,1);
					if (!neighbor.empty())
					{						
						tmp.at<uchar>(i, j) = neighbor[0][0];
					}
				}
			}
		}
		break;
		case 3://ảnh màu
		{
			for (int i = 0; i < imgPadding.rows; i++)
			{
				for (int j = 0; j < imgPadding.cols; j++)
				{
					vector<vector<int>> neighbor = getNeighbors(imgPadding, i, j, w, h,1);
					if (!neighbor.empty())
					{						
						tmp.at<Vec3b>(i, j)[0] = neighbor[0][0];
						tmp.at<Vec3b>(i, j)[1] = neighbor[1][0];
						tmp.at<Vec3b>(i, j)[2] = neighbor[2][0];
					}
				}
			}
		}
		break;
		default:
			return -1;
			break;
		}
		//Cắt ảnh theo định size ban đầu
		dropImage(tmp, destinationImage, w, h);
	}
	break;
	default:
		break;
	}
	return 0;
}

void convertImagePadding(Mat sourceImage, Mat & destinationImage, int option, int w, int h)
{
	//Chiều dài ảnh sẽ được cộng 2 lần h
	//Chiều rộng ảnh sẽ được cộng 2 lần w
	int newRows, newCols;
	newRows = sourceImage.rows + h * 2;
	newCols = sourceImage.cols + w * 2;
	Mat tmp(newRows,newCols,sourceImage.type(),Scalar(0));
	if (option == 0)
	{	
		if (sourceImage.channels() == 1)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(sourceImage.at<uchar>(i - h, j - w));
				}
			}
		}
		if (sourceImage.channels() == 3)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = saturate_cast<uchar>(
							sourceImage.at<Vec3b>(i - h, j - w)[k]);
					}
				}
			}
		}
		destinationImage = tmp;
		return;
	}
	if (option == 1)
	{
		if (sourceImage.channels() == 1)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols -w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(sourceImage.at<uchar>(i - h, j - w));
				}
			}
			//Xử lí viền dưới
			for (int i = newRows - h; i < newRows; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						sourceImage.at<uchar>(sourceImage.rows-1, c));
					c++;
				}
			}
			//Xử lí viền trên
			for (int i = 0; i < h; i++)
			{
				int c = 0;
				for (int j = w; j < newCols- w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						sourceImage.at<uchar>(0, c));
					c++;
				}
			}
			//Xử lí viền trái
			for (int i = 0; i < newRows; i++)
			{
				for (int j = 0; j < w; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						tmp.at<uchar>(i, w));
				}
			}
			//xử lí viền bên phải
			for (int i = 0; i < newRows; i++)
			{
				for (int j = newCols-w; j < newCols; j++)
				{
					tmp.at<uchar>(i, j) = saturate_cast<uchar>(
						tmp.at<uchar>(i, newCols - w-1));
				}
			}
		}
		if (sourceImage.channels() == 3)
		{
			for (int i = h; i < newRows - h; i++)
			{
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = saturate_cast<uchar>(
							sourceImage.at<Vec3b>(i - h, j - w)[k]);
					}
				}
			}

			//Xử lí viền dưới
			for (int i = newRows - h; i < newRows; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] =
							sourceImage.at<Vec3b>(sourceImage.rows - 1, c)[k];
					}
					c++;
				}
			}
			//Xử lí viền trên
			for (int i = 0; i < h; i++)
			{
				int c = 0;
				for (int j = w; j < newCols - w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = (
							sourceImage.at < Vec3b>(0, c)[k]);
					}
					c++;
				}
			}
			//Xử lí viền trái
			for (int i = 0; i < newRows; i++)
			{
				for (int j = 0; j < w; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = tmp.at<Vec3b>(i,w)[k];
					}
				}
			}
			//xử lí viền bên phải
			for (int i = 0; i < newRows; i++)
			{
				for (int j = newCols - w; j < newCols; j++)
				{
					for (int k = 0; k < 3; k++)
					{
						tmp.at<Vec3b>(i, j)[k] = (
							tmp.at<Vec3b>(i, newCols - w - 1)[k]);
					}
				}
			}
		}
		destinationImage = tmp;
		return;
	}
}

vector<vector<int>> getNeighbors(Mat sourceImg,int x, int y, int w, int h, int option)
{
	////Concept:
	//	Đầu vào là 1 pixel trên ảnh
	//	Đầu ra sẽ trả về mảng các giá trị xung quanh của 1 pixel truy vấn,
	//		tùy theo loại ảnh xám sẽ có 1 mảng các giá trị
	//		màu RGB sẽ có 3 mảng các giá trị
	//	Những pixel không thể áp filter w x h lên được sẽ trả về rỗng.

	//mảng các giá trị xung quanh của 1 pixel truy vấn
	vector<vector<int>> re;

	//Trả về rỗng với những điểm không áp filter được
	if (x < h / 2 - 0		//những điểm rìa dọc trái ( 0,1; 1,1; 2,1 .... (h/2 - 1),1)
		|| y < w / 2 - 0			//những điểm rìa trên ( 0,1; 0,2; 0,3 .... 0,(w/2-1))
		|| x>sourceImg.rows - 1 - h / 2			//những điểm rìa rìa dưới
		|| y > sourceImg.cols - 1 - w / 2)			////những điểm dọc phải
		return vector<vector<int>>();

	//Xử lí theo loại ảnh
	switch (sourceImg.channels())
	{
	case 1://Grayscale
	{
		vector<int> gArr;
		int i, k, j, ni, nj, value;
		bool isChanged = false;

		i = (x - h / 2);
		ni = (x + h / 2);
		nj = (y + w / 2);
		k = (y - w / 2);

		for (i; i <= ni; i++)
		{
			j = k;
			for (j; j <= nj; j++)
			{
				switch (option)
				{
				case 1://ascend
				{

					value = (int)sourceImg.at<uchar>(i, j);
					if (gArr.empty())
					{
						gArr.push_back(value);
					}
					else
					{
						vector<int>::iterator c;
						for (c = gArr.begin(); c < gArr.end(); c++)
						{
							if (value < *c)
							{
								gArr.insert(c, value);
								break;
							}

						}

						if (c == gArr.end())
							gArr.insert(gArr.end(), value);
					}
				}
				break;
				case 2://normal
				{
					gArr.push_back((int)sourceImg.at<uchar>(i, j));
				}
				break;
				default:
					break;
				}
			}
		}

		re.push_back(gArr);
		return re;
	}
	break;
	case 3://RGB
	{
		vector<int> gArr1, gArr2, gArr3;
		int i, k, j, ni, nj;
		int value1, value2, value3;

		i = (x - h / 2);
		ni = (x + h / 2);
		nj = (y + w / 2);
		k = (y - w / 2);

		for (i; i <= ni; i++)
		{
			j = k;
			for (j; j <= nj; j++)
			{
				switch (option)
				{
				case 1://ascend
				{
					value1 = sourceImg.at<Vec3b>(i, j)[0];
					value2 = sourceImg.at<Vec3b>(i, j)[1];
					value3 = sourceImg.at<Vec3b>(i, j)[2];

					if (gArr1.empty())
					{
						gArr1.push_back(value1);
					}
					else
					{
						vector<int>::iterator c1;
						for (c1 = gArr1.begin(); c1 < gArr1.end(); c1++)
						{
							if (value1 < *c1)
							{
								gArr1.insert(c1, value1);
								break;
							}
						}
						if (c1 == gArr1.end())
							gArr1.insert(gArr1.end(), value1);
					}

					if (gArr2.empty())
					{
						gArr2.push_back(value2);
					}
					else
					{
						vector<int>::iterator c2;
						for (c2 = gArr2.begin(); c2 < gArr2.end(); c2++)
						{
							if (value2 < *c2)
							{
								gArr2.insert(c2, value2);
								break;
							}
						}
						if (c2 == gArr2.end())
							gArr2.insert(gArr2.end(), value2);
					}

					if (gArr3.empty())
					{
						gArr3.push_back(value3);
					}
					else
					{
						vector<int>::iterator c3;
						for (c3 = gArr3.begin(); c3 < gArr3.end(); c3++)
						{
							if (value3 < *c3)
							{
								gArr3.insert(c3, value3);
								break;
							}
						}
						if (c3 == gArr3.end())
							gArr3.insert(gArr3.end(), value3);
					}

				}
				break;
				case 2://normal
				{
					gArr1.push_back((int)sourceImg.at<Vec3b>(i, j)[0]);
					gArr2.push_back((int)sourceImg.at<Vec3b>(i, j)[1]);
					gArr3.push_back((int)sourceImg.at<Vec3b>(i, j)[2]);
				}
				break;
				default:
					break;
				}

			}
		}
		//vec(gArr1);
		/*vec(gArr2);
		vec(gArr3);*/
		re.push_back(gArr1);
		re.push_back(gArr2);
		re.push_back(gArr3);
		return re;
	}
	break;
	default:
		break;
	}
	return re;
}

void dropImage(Mat sourceImage, Mat & destinationImage, int w, int h)
{
	//Xóa các cạnh thêm với 2 lần w, 2 lần h
	Mat tmp(sourceImage.rows - h * 2, sourceImage.cols - 2 * w, sourceImage.type());

	//Gán lại giá trị
	if (sourceImage.channels() == 1)
	{
		for (int i = 0; i < tmp.rows; i++)
		{
			for (int j = 0; j < tmp.cols; j++)
			{
				tmp.at<uchar>(i, j) = 
					saturate_cast<uchar>(sourceImage.at<uchar>(i + h, j + w));
			}
		}
	}
	if (sourceImage.channels() == 3)
	{
		int valueI, valueJ;
		for (int i = 0; i < tmp.rows; i++)
		{
			for (int j = 0; j < tmp.cols; j++)
			{
				valueI = i + h,valueJ = j + w;
				tmp.at<Vec3b>(i, j)[0] = saturate_cast<uchar>(
					sourceImage.at<Vec3b>(valueI, valueJ)[0]);

				tmp.at<Vec3b>(i, j)[1] = saturate_cast<uchar>(
					sourceImage.at<Vec3b>(valueI, valueJ)[1]);

				tmp.at<Vec3b>(i, j)[2] = saturate_cast<uchar>(
					sourceImage.at<Vec3b>(valueI, valueJ)[2]);
			}
		}
		destinationImage = tmp;
	}
	destinationImage = tmp;
}