﻿#include"lab02.h"

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		cout << "Tham so khong hop le!\n";
		return -1;
	}


	Mat image, img;
	image = imread(argv[1]);
	//Kiểm tra ảnh màu và rgb hợp lệ
	if (image.type() != CV_8UC3 && image.type() != CV_8UC1)
	{
		cout << "Anh truyen vao khong phai anh grayscale hay rgb, xin nhap lai!\n";
		return  -1;
	}
	namedWindow("Original", WINDOW_AUTOSIZE);
	imshow("Original", image);
	
	if (strcmp(argv[2], "1") == 0)
	{
		int w = atoi(argv[3]);
		int h = atoi(argv[4]);
		int option = atoi(argv[5]);
		averageFilter(image, img, w, h, option);
	}

	if (strcmp(argv[2], "2") == 0)
	{
		int w = atoi(argv[3]);
		int h = atoi(argv[4]);
		int option = atoi(argv[6]);
		int sigma = atoi(argv[5]);
		gaussianFilter(image, img, w, h, sigma, option);
	}

	if (strcmp(argv[2], "3") == 0)
	{
		int w = atoi(argv[3]);
		int h = atoi(argv[4]);
		int option = atoi(argv[6]);
		int name = atoi(argv[5]);
		orderStatisticFilter(image, img, w, h, name, option);
	}

	namedWindow("Out", WINDOW_AUTOSIZE);
	imshow("Out", img);
	waitKey();
	return 1;
}