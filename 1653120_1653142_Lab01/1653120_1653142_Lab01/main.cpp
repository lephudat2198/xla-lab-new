#include"Lab01.h"

void main(int argc, char** argv)
{

	if (argc < 2)
	{
		cout << "Tham so truyen khong hop le!\n";
		return;
	}
	cout << argv[2] << endl;
	if (strcmp(argv[2], "1") == 0)
	{
		cout << "Bien doi anh mau thanh anh xam\n";
		Mat image,imageout;
		image = imread(argv[1],IMREAD_COLOR);
			namedWindow("Original image", WINDOW_AUTOSIZE);
			imshow("Original image", image);
		if (isGrayimg(image))
		{
			cout << "Khong phai anh rgb\n";
			return;
		}
		if (rgbToGray(image, imageout) != -1)
		{
			namedWindow("Output", WINDOW_AUTOSIZE);
			imshow("Output", imageout);
			waitKey();
		}
		return;
	}
	if (strcmp(argv[2], "2") == 0)
	{
		Mat image, imageout;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		int beta = atoi(argv[3]);
		adjustBrightness(image, imageout, beta);
		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", imageout);
		waitKey();
		return;
	}
	if (strcmp(argv[2], "3") == 0)
	{

		Mat image, imageout;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		int alpha = atoi(argv[3]);
		adjustContrast(image, imageout, alpha);
		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", imageout);
		waitKey();
		return;
	}
	if (strcmp(argv[2], "4") == 0)
	{
		Mat image, imageout;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		transNegative(image, imageout);
		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", imageout);
		waitKey();
		return;
		return;
	}
	if (strcmp(argv[2], "5") == 0)
	{
		Mat image, imageout;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		int c = atoi(argv[3]);
		transLog(image, imageout, c);
		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", imageout);
		waitKey();
		return;
	}
	if (strcmp(argv[2], "6") == 0)
	{
		Mat image, imageout;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		int c = atoi(argv[3]);
		double gamma = stod(argv[4]);
		transGamma(image, imageout, c,gamma);
		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", imageout);
		waitKey();
		return;
	}
	if (strcmp(argv[2], "7") == 0)
	{
		vector<Mat> hists;
		Mat image;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		calcHistgogram(image, hists);
		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		waitKey();
		return;
	}
	if (strcmp(argv[3], "8") == 0)
	{
		Mat image, image2;
		image = imread(argv[1], IMREAD_COLOR);
		image2 = imread(argv[2], IMREAD_COLOR);
		if (!isRGBimg(image) && !isGrayimg(image) || !isRGBimg(image2) && !isGrayimg(image2))
		{
			cout << "Khong phai anh rgb va grayscale\n";
			return;
		}
		
		cout <<"By chisquare: "<< compareHist(image, image2)<<endl;

		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", image2);
		waitKey();
		return;
	}
	if (strcmp(argv[2], "10") == 0)
	{
		Mat image, image2;
		image = imread(argv[1], IMREAD_GRAYSCALE);
		if (!isGrayimg(image))
		{
			cout << "Khong phai anh grayscale\n";
			return;
		}
		int gBin = atoi(argv[3]);
		calcHistogram(image, gBin);

		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		waitKey();
		return;
	}
	if (strcmp(argv[2], "9") == 0)
	{
		Mat image, image2;
		image = imread(argv[1], IMREAD_COLOR);
		if (!isRGBimg(image))
		{
			cout << "Khong phai anh RGB\n";
			return;
		}
		int rBin = atoi(argv[3]);
		int gBin = atoi(argv[4]);
		int bBin = atoi(argv[5]);
		calcHistogram(image,rBin,gBin,bBin);

		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		waitKey();
		return;
	}
	if (strcmp(argv[3], "12") == 0)
	{
		Mat image, image2;
		image = imread(argv[1], IMREAD_GRAYSCALE);
		image2 = imread(argv[2], IMREAD_GRAYSCALE);

		int gBin = atoi(argv[4]);
		cout << "OK!\n";
		cout << "By chisquare: " << compareHist(image, image2,gBin) << endl;

		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", image2);
		waitKey();
		return;
	}
	if (strcmp(argv[3], "11") == 0)
	{
		Mat image, image2;
		image = imread(argv[1], IMREAD_COLOR);
		image2 = imread(argv[2], IMREAD_COLOR);

		int rBin = atoi(argv[4]);
		int gBin = atoi(argv[5]);
		int bBin = atoi(argv[6]);

		cout << "By chisquare: " << compareHist(image, image2, rBin,gBin,bBin) << endl;

		namedWindow("Original image", WINDOW_AUTOSIZE);
		imshow("Original image", image);
		namedWindow("Output", WINDOW_AUTOSIZE);
		imshow("Output", image2);
		waitKey();
	}
}
