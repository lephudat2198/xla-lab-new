﻿#pragma once

#include"Libraries/Headers/opencv2/core.hpp"
#include"Libraries/Headers/opencv2/imgproc.hpp"
#include"Libraries/Headers/opencv2/highgui.hpp"


#include<iostream>
using namespace std;
using namespace cv;


bool isRGBimg(Mat srcImg);
//Kiểm tra ảnh có phải là Grayscale
bool isGrayimg(Mat srcImg);
//Biến đổi ảnh màu thành độ xám
//	(Nếu là ảnh xám thì không thực
//	hiện thao tác này, xuất thông báo
//	chỉ thực hiện thao tác với ảnhz//	màu)
int rgbToGray(Mat srcImg, Mat &dstImg);

//Thay đổi độ sáng ảnh màu / ảnh độ xám.
int adjustBrightness(Mat srcImg, Mat &dstImg, int beta);

//Thay đổi độ tương phản ảnh màu / ảnh độ xám.
int adjustContrast(Mat srcImg,	Mat &dstImg, int alpha);

//Tạo ảnh âm bản. 
int transNegative(Mat srcImg,Mat &dstImg);

//Biến đổi ảnh Log Transform. 
int transLog(Mat srcImg, Mat &dstImg, int c);

//Biến đổi ảnh Gamma Transform. 
int transGamma(Mat srcImg, Mat	&dstImg, int c, double gamma);

//Tính histogram của 1 ảnh màu hoặc xám
int calcHistgogram(Mat srcImg,
	vector<Mat> &hists);

//So sánh 2 ảnh dựa vào histogram 
double compareHist(Mat
	srcImg1, Mat srcImg2);

//Trả về mảng range x num. intensity
bool getImgIntensity(Mat srcImg, vector<int**> &hists);

//trả về intensity xuất hiện nhiều nhất
int maxValue(int **srcArray,int n);

//tạo ra hình histogram
bool generateHistogram(Mat &dest, int **ImgIntensity);

//Tính histogram lượng hóa màu của ảnh RGB
vector<Mat>  calcHistogram(Mat srcImg, int rBin, int gBin, int bBin);

//Tính histogram lượng hóa màu của ảnh xám
vector<Mat> calcHistogram(Mat srcImg, int gBin);

//So ánh 2 ảnh dựa vào lược đồ lượng hóa ảnh màu
double compareHist(Mat srcImg1, Mat srcImg2, int rBin, int gBin, int bBin);

//So ánh 2 ảnh dựa vào lược đồ lượng hóa ảnh xám
double compareHist(Mat srcImg1, Mat srcImg2, int gBin);

//Cân bằng histogram.
int equalizeHistogram(Mat srcImg, Mat dstImg);