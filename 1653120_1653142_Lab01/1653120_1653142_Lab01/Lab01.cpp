﻿#include "Lab01.h"

#define range 256
#define whist 256
#define hhist 400

//Kiểm tra ảnh có phải là RGB
bool isRGBimg(Mat srcImg)
{
	if(srcImg.channels() == 3)
		return true;
	return false;
}
//Kiểm tra ảnh có phải là Grayscale
bool isGrayimg(Mat srcImg)
{
	if (srcImg.channels() == 1)
		return true;
	return false;
}

int rgbToGray(Mat srcImg, Mat &dstImg)
{
	if (!isRGBimg(srcImg))
	{
		cout << "Khong phai loai anh RGB!\n";
		return -1;
	}

	//Ảnh trắng đen, khởi tạo với size như ảnh gốc
	Mat srcImgGray(srcImg.rows, srcImg.cols, CV_8UC1, Scalar(0));
	for (int i = 0; i < srcImg.rows; i++)
	{
		for (int j = 0; j < srcImg.cols; j++)
		{

			double redChannel = srcImg.at<Vec3b>(i, j)[2];
			double greenChannel = srcImg.at<Vec3b>(i, j)[1];
			double blueChannel = srcImg.at<Vec3b>(i, j)[0];

			double chs = redChannel * 0.299 + greenChannel * 0.587 + blueChannel * 0.114;

			srcImgGray.at<uchar>(i, j) = (int)chs;
		}
	}
	dstImg = srcImgGray;
	return 1;
}

int adjustBrightness(Mat srcImg, Mat & dstImg, int beta)
{

	//Kiểm tra loại ảnh, 1 channel là grayscale, 3 channels là RGB
	if (isGrayimg(srcImg))
	{
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				tmp.at<uchar>(y, x) =
					saturate_cast<uchar>(srcImg.at<uchar>(y, x) + beta);
			}
		}
		dstImg = tmp;
		return 1;
	}
	if (isRGBimg(srcImg))
	{
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC3);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int c = 0; c < srcImg.channels(); c++)
					tmp.at<Vec3b>(y, x)[c] =
					saturate_cast<uchar>(srcImg.at<Vec3b>(y, x)[c] + beta);
			}
		}
		dstImg = tmp;
		return 1;
	}
	cout << "Khong phai loai anh trang den, mau\n";
	return -1;
}

int adjustContrast(Mat srcImg, Mat & dstImg, int alpha)
{
	//Kiểm tra loại ảnh, 1 channel là grayscale, 3 channels là RGB
	if (isRGBimg(srcImg))
	{
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC3);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int c = 0; c < srcImg.channels(); c++)
				{
					tmp.at<Vec3b>(y, x)[c] =
						saturate_cast<uchar>(alpha*srcImg.at<Vec3b>(y, x)[c]);
				}
			}
		}
		dstImg = tmp;
		return 1;
	}
	if (isGrayimg(srcImg))
	{
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				tmp.at<uchar>(y, x) =
					saturate_cast<uchar>(alpha*srcImg.at<uchar>(y, x));

			}
		}
		dstImg = tmp;
		return 1;
	}

	cout << "Khong phai loai anh trang den, mau\n";
	return -1;
}

int transNegative(Mat srcImg, Mat & dstImg)
{
	double r, s;
	//Kiểm tra loại ảnh, 1 channel là grayscale, 3 channels là RGB
	if (isRGBimg(srcImg))
	{
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC3);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int c = 0; c < srcImg.channels(); c++)
				{
					r =saturate_cast<uchar>( srcImg.at<Vec3b>(y, x)[c]);	
					s = (range - 1 - r);
					tmp.at<Vec3b>(y, x)[c]=(int)s;
				}
			}
		}
		dstImg = tmp;
		return 1;
	}
	if (isGrayimg(srcImg))
	{
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				r = srcImg.at<uchar>(y, x);
				s = (range - 1 - r);
				tmp.at<uchar>(y, x)=(int)s;
			}
		}
		dstImg = tmp;
		return 1;
	}
	return -1;
}

void transLogSingleChannel(Mat srcImg, Mat & dstImg, int c)
{
	double r, s, min, max;

	double ** arr = new double*[srcImg.cols];
	Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
	for (int y = 0; y < srcImg.rows; y++)
	{
		arr[y] = new double[srcImg.cols];
		for (int x = 0; x < srcImg.cols; x++)
		{
			r = saturate_cast<uchar>(srcImg.at<uchar>(y, x));
			if (r < 0)
				r = 0;
			s = c * log(1 + r);

			if (y == 0 && x == 0)
			{
				min = max = s;
			}

			if (s < min)
				min = s;
			if (s > max)
				max = s;

			arr[y][x] = s;
		}
	}


	for (int y = 0; y < srcImg.rows; y++)
	{
		for (int x = 0; x < srcImg.cols; x++)
		{
			tmp.at<uchar>(y, x) = (range* arr[y][x]) / (max - min);
			//cout << (double)saturate_cast<uchar>(tmp.at<uchar>(y, x)) << endl;
		}
	}
	dstImg = tmp;
}

int transLog(Mat srcImg, Mat & dstImg, int c)
{
	//Kiểm tra loại ảnh, 1 channel là grayscale, 3 channels là RGB
	if (isGrayimg(srcImg))
	{
		transLogSingleChannel(srcImg, dstImg, c);
		return 1;
	}
	if (isRGBimg(srcImg))
	{
		Mat *rgb = new Mat[3];
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
		for (int i = 0; i < 3; i++)
		{
			rgb[i] = tmp;
		}
		//chuyển hình RGB sang 3 hình r,g,b
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int k = 0; k < srcImg.channels(); k++)
				{
					rgb[k].at<uchar>(y, x) = srcImg.at<Vec3b>(y, x)[k];
				}
			}
		}

		//Gamma từng ảnh r,g,b
		transLogSingleChannel(rgb[0], rgb[0], c);

		transLogSingleChannel(rgb[1], rgb[1], c);

		transLogSingleChannel(rgb[2], rgb[2], c);



		//gộp 3 ảnh lại thành 1 ảnh RGB
		Mat result(srcImg.rows, srcImg.cols, CV_8UC3);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int k = 0; k < srcImg.channels(); k++)
				{
					result.at<Vec3b>(y, x)[k] = saturate_cast<uchar>( rgb[k].at<uchar>(y, x));
				}
			}
		}
		dstImg = result;
		return 1;
	}
	return -1;
}

void transGammaSinglechannel(Mat srcImg, Mat &dstImg, int c, double gamma)
{
	double min = -1, max = -1,r,s;
	double ** arr = new double*[srcImg.cols];
	Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
	for (int y = 0; y < srcImg.rows; y++)
	{
		arr[y] = new double[srcImg.cols];
		for (int x = 0; x < srcImg.cols; x++)
		{
			r = (int)srcImg.at<uchar>(y, x);
			s = c * pow(r, gamma);
			if (y == 0 && x == 0)
				min = s;
			if (s < min)
				min = s;
			if (s > max)
				max = s;
			arr[y][x] = s;
		}
	}
	cout << min << " " << max << endl;
	for (int y = 0; y < srcImg.rows; y++)
	{
		for (int x = 0; x < srcImg.cols; x++)
		{

			tmp.at<uchar>(y, x) = (int)(range*1.0 * arr[y][x]) / (max - min);
		}
	}
	dstImg = tmp;
}

int transGamma(Mat srcImg, Mat &dstImg, int c, double gamma)
{
	double r, s;
	//Kiểm tra loại ảnh, 1 channel là grayscale, 3 channels là RGB
	if (isRGBimg(srcImg))
	{
		Mat *rgb = new Mat[3];
		Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
		for (int i = 0; i < 3; i++)
		{
			rgb[i]=tmp;
		}
		//chuyển hình RGB sang 3 hình r,g,b
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int k = 0; k < srcImg.channels(); k++)
				{
					rgb[k].at<uchar>(y,x) = srcImg.at<Vec3b>(y, x)[k];
				}
			}
		}

		//Gamma từng ảnh r,g,b
		transGammaSinglechannel(srcImg, rgb[0], c, gamma);
		transGammaSinglechannel(srcImg, rgb[1], c, gamma);
		transGammaSinglechannel(srcImg, rgb[2], c, gamma);

		//gộp 3 ảnh lại thành 1 ảnh RGB
		Mat result(srcImg.rows, srcImg.cols, CV_8UC3);
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				for (int k = 0; k < srcImg.channels(); k++)
				{
					result.at<Vec3b>(y, x)[k] = rgb[k].at<uchar>(y, x);
				}
			}
		}
		dstImg = result;
		return 1;
	}
	if (isGrayimg(srcImg))
	{
		transGammaSinglechannel(srcImg, dstImg , c, gamma);
	}
	return -1;
}

//Trả về mảng range x num. intensity
bool getImgIntensity(Mat srcImg,vector<int**> &hists)
{
	if (isGrayimg(srcImg))
	{		
		int ** hist = new int*[range];
		for (int i = 0; i < range; i++)
		{
			hist[i] = new int[1];
			hist[i][0] = 0;
		}
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{
				hist[srcImg.at<uchar>(y, x)][0]++;
			}
		}
		hists.push_back(hist);
		return true;
	}
	if (isRGBimg(srcImg))
	{
		int ** rhist = new int*[range];
		int ** ghist = new int*[range];
		int ** bhist = new int*[range];
		for (int i = 0; i < range; i++)
		{
			rhist[i] = new int[1];
			rhist[i][0] = 0;
			ghist[i] = new int[1];
			ghist[i][0] = 0;
			bhist[i] = new int[1];
			bhist[i][0] = 0;
		}
		for (int y = 0; y < srcImg.rows; y++)
		{
			for (int x = 0; x < srcImg.cols; x++)
			{

				rhist[srcImg.at<Vec3b>(y, x)[2]][0]++;

				ghist[srcImg.at<Vec3b>(y, x)[1]][0]++;

				bhist[srcImg.at<Vec3b>(y, x)[0]][0]++;

			}
		}
		hists.push_back(rhist);
		hists.push_back(ghist);
		hists.push_back(bhist);
		return true;
	}
	return false;
}

//trả về intensity xuất hiện nhiều nhất
int maxValue(int **srcArray, int n)
{
	int tmp = -1;
	if (srcArray == NULL)
		return tmp;
	tmp = srcArray[0][0];
	for (int i = 1; i <n; i++)
		if (tmp < srcArray[i][0])
			tmp = srcArray[i][0];
	return tmp;
}

int sum(int **srcArray)
{
	int tmp = 0;
	if (srcArray == NULL)
		return tmp;
	tmp = srcArray[0][0];
	for (int i = 1; i < range; i++)
			tmp += srcArray[i][0];
	return tmp;
}
//tạo ra hình histogram
bool generateHistogram(Mat &dest, int **ImgIntensity)
{
	//lấy inten xuất hiện nhiều nhất
	int maxHist = maxValue(ImgIntensity,range);

	//sketch vừa màn hình whist x hhist
	int div = maxHist / hhist;

	Mat graysImg(hhist, whist, CV_8UC1, Scalar(0));
	for (int i = 0; i < range; i++)
	{
		line(graysImg, Point(i, maxHist / div), 
			Point(i, maxHist/ div - ImgIntensity[i][0] / div), 
			Scalar(255), 1, 8, 0);
	}
	dest = graysImg;
	return true;
}

int calcHistgogram(Mat srcImg, vector<Mat>& hists)
{
	if (isGrayimg(srcImg))
	{
		//lấy thông tin histogram
		vector<int**> histsInten;
		Mat hist;
		getImgIntensity(srcImg, histsInten);
		generateHistogram(hist, histsInten[0]);
		hists.push_back(hist);

		namedWindow("a", WINDOW_AUTOSIZE);
		imshow("a", hist);
		return 0;
	}
	if (isRGBimg(srcImg))
	{
		//lấy thông tin histogram
		vector<int**> histsInten;
		for (int i = 0; i < srcImg.channels(); i++)
		{
			Mat hist;
			getImgIntensity(srcImg, histsInten);
			generateHistogram(hist, histsInten[i]);
			hists.push_back(hist);
		}

		namedWindow("1", WINDOW_AUTOSIZE);
		imshow("1", hists[0]);
		namedWindow("2", WINDOW_AUTOSIZE);
		imshow("2", hists[1]);
		namedWindow("3", WINDOW_AUTOSIZE);
		imshow("3", hists[2]);
		return 0;
	}
	return -1;
}

double calcByChisquare(int inten1, int inten2)
{
	double value = (inten1 - inten2)*(inten1 - inten2);
	//cout << value << endl;
	return (value/(inten1+inten2));
}

double compareHist(Mat srcImg1, Mat srcImg2)
{
	vector<int**>srcArray1, srcArray2;
	double result = 0;
	if (isGrayimg(srcImg1) && isGrayimg(srcImg2))
	{
		getImgIntensity(srcImg1, srcArray1);
		getImgIntensity(srcImg2, srcArray2);
		int size1 = sum(srcArray1[0]);
		int size2 = sum(srcArray2[0]);
		for (int i = 0; i < range; i++)
		{
			if (srcArray1[0][i][0] != 0)
			{
				result += calcByChisquare((srcArray1[0][i][0]*1.0/size1), (srcArray2[0][i][0] * 1.0 / size1));
			}
		}
		return result;
	}
	if (isRGBimg(srcImg1) && isRGBimg(srcImg2))
	{
		getImgIntensity(srcImg1, srcArray1);
		getImgIntensity(srcImg2, srcArray2);
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < range; i++)
			{
				int size1 = sum(srcArray1[0]);
				int size2 = sum(srcArray2[0]);
				if (srcArray1[j][i][0] != 0)
				{
					result += calcByChisquare((srcArray1[j][i][0]), (srcArray2[j][i][0]));
				}
			}
		}
		return result/3;
	}
	return -1.0;
}

void calcValue(Mat srcImg, int *&hist,int &rage,int gBin)
{
	rage = range / gBin;
	hist = new int[gBin];
	for (int i = 0; i < gBin; i++)
		hist[i] = 0;
	for (int y = 0; y < srcImg.rows; y++)
	{
		for (int x = 0; x < srcImg.cols; x++)
		{
			for (int k = 0; k < gBin; k++)
			{
				if ((int)srcImg.at<uchar>(y, x) >= (k * rage) &&
					(int)srcImg.at<uchar>(y, x) < ((k + 1) * rage))
				{
					hist[k]++;
					break;
				}
			}
		}
	}
}
int Max(int*arr, int n)
{
	int max = -1;
	for (int i = 0; i < n; i++)
		if (max < arr[i])
			max = arr[i];
	return max;
}
Mat pre_calcHistogram(Mat srcImg, int gBin)
{
	Mat hists;
	int rage;
	int *hist;
	calcValue(srcImg, hist, rage, gBin);
	int max = Max(hist, gBin);
	int div = max / 400;
	for (int i = 0; i < gBin; i++)
	{
		cout << hist[i] << "\t";
		cout << hist[i] / div << endl;
	}
	Mat histogram(400, 256, CV_8UC1, Scalar(0));
	if (div < 1)
		div = 1;


	int j = 0;
	for (int i = 0; i < gBin; i++)
	{
		for (j; j < range; j++)
		{
			if (j > i*rage)
			{
				j = i * rage;
				break;
			}
			line(histogram, Point(j, max / div),
				Point(j, (max / div) - (hist[i] / div)),
				Scalar(255), 1, 8, 0);
		}
	}
	return histogram;
}
//trả về mảng frequency của các bins
vector<Mat>  calcHistogram(Mat srcImg, int rBin, int gBin, int bBin)
{
	vector<Mat> hists;
	Mat *rgb = new Mat[3];
	Mat tmp(srcImg.rows, srcImg.cols, CV_8UC1);
	for (int i = 0; i < 3; i++)
	{
		rgb[i] = tmp;
	}
	//chuyển hình RGB sang 3 hình r,g,b
	for (int y = 0; y < srcImg.rows; y++)
	{
		for (int x = 0; x < srcImg.cols; x++)
		{
			for (int k = 0; k < srcImg.channels(); k++)
			{
				rgb[k].at<uchar>(y, x) = srcImg.at<Vec3b>(y, x)[k];
			}
		}
	}

	//Gamma từng ảnh r,g,b
	
	rgb[2] = pre_calcHistogram(rgb[2], rBin);
	rgb[1] = pre_calcHistogram(rgb[1], gBin);
	rgb[0] = pre_calcHistogram(rgb[0], bBin);


	namedWindow("1", WINDOW_AUTOSIZE);
	imshow("1", rgb[0]);
	namedWindow("2", WINDOW_AUTOSIZE);
	imshow("2", rgb[1]);
	namedWindow("3", WINDOW_AUTOSIZE);
	imshow("3", rgb[2]);
	
	hists.push_back(rgb[0]);
	hists.push_back(rgb[1]);
	hists.push_back(rgb[2]);

	return hists;
}

//trả về mảng frequency của các bins
vector<Mat> calcHistogram(Mat srcImg, int gBin)
{
	vector<Mat> tmp;
	tmp.push_back(pre_calcHistogram(srcImg, gBin));

	namedWindow("a", WINDOW_AUTOSIZE);
	imshow("a", tmp[0]);
	return tmp;
}

double compareHist(Mat srcImg1, Mat srcImg2, int rBin, int gBin, int bBin)
{
	//lấy mảng các mức sáng
	//double **ghist1 = calcHistogram(srcImg1, rBin,gBin,bBin);
	//double **ghist2 = calcHistogram(srcImg1, rBin, gBin, bBin);
	double value1 = 0;
	double value2 = 0;
	double value3 = 0;
	double value11 = 0;
	double value22= 0;
	double value33 = 0;
	//dựa vào frequency tính độ compare
	int *hist1, *hist2, *hist3,*hist11, *hist22, *hist33;
	int rage1, rage2,rage3,rage11, rage22,rage33;


	Mat *rgb = new Mat[3];
	Mat tmp(srcImg1.rows, srcImg1.cols, CV_8UC1);
	for (int i = 0; i < 3; i++)
	{
		rgb[i] = tmp;
	}
	//chuyển hình RGB sang 3 hình r,g,b
	for (int y = 0; y < srcImg1.rows; y++)
	{
		for (int x = 0; x < srcImg1.cols; x++)
		{
			for (int k = 0; k < srcImg1.channels(); k++)
			{
				rgb[k].at<uchar>(y, x) = srcImg1.at<Vec3b>(y, x)[k];
			}
		}
	}


	Mat *rgb1 = new Mat[3];
	Mat tmp1(srcImg2.rows, srcImg2.cols, CV_8UC1);
	for (int i = 0; i < 3; i++)
	{
		rgb1[i] = tmp1;
	}
	//chuyển hình RGB sang 3 hình r,g,b
	for (int y = 0; y < srcImg2.rows; y++)
	{
		for (int x = 0; x < srcImg2.cols; x++)
		{
			for (int k = 0; k < srcImg2.channels(); k++)
			{
				rgb1[k].at<uchar>(y, x) = srcImg2.at<Vec3b>(y, x)[k];
			}
		}
	}

	calcValue(rgb[0], hist1, rage1, bBin);
	calcValue(rgb[0], hist11, rage11,bBin);
	calcValue(rgb[1], hist2, rage2, gBin);
	calcValue(rgb[1], hist22, rage22, gBin);
	calcValue(rgb[2], hist3, rage3, rBin);
	calcValue(rgb[2], hist33, rage33, rBin);

	for (int i = 0; i < gBin; i++)
	{
		value2 += calcByChisquare(hist2[i], hist22[i]);
	}
	for (int i = 0; i < rBin; i++)
	{
		value3 += calcByChisquare(hist3[i], hist33[i]);
	}
	for (int i = 0; i < bBin; i++)
	{
		value1 += calcByChisquare(hist1[i], hist11[i]);
	}

	return (value1+value2+value3)/3;
}

double compareHist(Mat srcImg1, Mat srcImg2, int gBin)
{
	
	double value = 0;
	int *hist1, *hist2;
	int rage1, rage2,size1,size2;

	calcValue(srcImg1, hist1, rage1, gBin);
	calcValue(srcImg2, hist2, rage2, gBin);

	for (int i = 0; i < gBin; i++)
	{
		value += calcByChisquare(hist1[i], hist2[i]);
	}
	return value;
}

double* calcValueHistogramEqualization(double *pk)
{
	double * pr = new double[range];
	for (int i = 0; i < range; i++)
	{
		pr[i] = 0;
		for (int j = 0; j <= i; j++)
		{
			pr[i] += 7 * pk[j];
		}
	}
	return pr;
}

void roundValue(double*VHL, int*nk, int*&mk, double*&ps, int *&sk)
{
	sk = new int[range];
	for (int i = 0; i < range; i++)
	{
		sk[i] = 0;
		mk[i] = 0;
		ps[i] = 0;
	}
	
	 

}

int equalizeHistogram(Mat srcImg, Mat dstImg)
{
	vector<int**>nk;
	double pk;
	getImgIntensity(srcImg, nk);
	double * pr = new double[range];
	double summ = 1.0*sum(nk[0]);
	for (int i = 0; i < range; i++)
	{
		pr[i] = nk[0][i][0] / summ;
	}

	return 0;
}
